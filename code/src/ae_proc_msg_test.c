/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTOS LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *
 *          This software is subject to an open source license and 
 *          may be freely redistributed under the terms of MIT License.
 ****************************************************************************
 */

/**************************************************************************//**
 * @file        ae_proc_mem_priviledge_test.c
 * @brief       test memory api and previledge issues
 *              
 * @version     V1.2021.02
 * @authors     JL
 * @date        2021 Feb
 * @note        Each process is in an infinite loop. Processes never terminate.
 * @details
 *
 *****************************************************************************/

#include "rtx.h"
#include "uart_polling.h"
#include "ae_proc.h"
#include "printf.h"
#include <LPC17xx.h>

int ok_counter = 0;
int total_test = 10;
/**************************************************************************//**
 * @brief: a process that prints two lines of five uppercase letters
 *         and then changes P2's priority to HIGH
 *         and then yields the cpu.
 *****************************************************************************/
void proc1(void)
{	
	int retval;
	MSG_BUF* envlope;
	
	envlope = request_memory_block();
	retval = send_message(2, (void*) envlope);
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 3 OK\r\n"); //test3: can assign msg to existing proc
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 3 FAIL\n\r");
	}
	
	envlope = request_memory_block();
	retval = send_message(225, (void*) envlope);
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 4 OK\r\n"); //test4: expect error when sending to non-existent proc
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 3 FAIL\n\r");
	}
	envlope = NULL;
	retval = send_message(2, (void*) envlope);
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 5 OK\r\n"); //test5: expect error when envelop is null
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 5 FAIL\n\r");
	}
	MSG_BUF* p_msg_env = (MSG_BUF*) request_memory_block();
  p_msg_env->mtype = KCD_REG;
	p_msg_env->mtext[0] = '%';
	p_msg_env->mtext[1] = 'W';
	p_msg_env->mtext[2] = '\0';
	retval = send_message(2, (void*) p_msg_env);
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 6 OK\r\n"); //test6: expect inserting multiple msg okay
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 6 FAIL\n\r");
	}
	while ( 1 ) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief  a process that prints five numbers, change P1's priority to HIGH
 *         and then yields the cpu.
 *****************************************************************************/
void proc2(void)
{
	uart1_put_string("G005_test: START\r\n"); 
	printf("G005_test: total %d tests\r\n", total_test); 
	int sender_id;
	int *sender_id_ptr = &sender_id;
  *sender_id_ptr = 123;
	void* env = receive_message(sender_id_ptr);
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 1 OK\r\n"); //test1: expect receives message properly
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 1 FAIL\n\r");
	}
	if (release_memory_block(env) == RTX_OK) {
		uart1_put_string("G005_test: test 2 OK\r\n"); //test2: expect releases memory block OK
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 2 FAIL\n\r");
	}
	
	set_process_priority(PID_P2, MEDIUM);
	*sender_id_ptr = 123;
	env = receive_message(sender_id_ptr);
	if (env != NULL && *sender_id_ptr == 1 && ((MSG_BUF*)env)->mtype == KCD_REG &&
		((MSG_BUF*)env)->mtext[0] == '%' && ((MSG_BUF*)env)->mtext[1] == 'W' &&
	  ((MSG_BUF*)env)->mtext[2] == '\0') {
		uart1_put_string("G005_test: test 7 OK\r\n"); //test7: expect receives 2nd message properly
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 7 FAIL\n\r");
	}
		if (release_memory_block(env) == RTX_OK) {
		uart1_put_string("G005_test: test 8 OK\r\n"); //test8: expect releases memory block OK
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 8 FAIL\n\r");
	}
	
	MSG_BUF* p_msg_env = (MSG_BUF*) request_memory_block();
  p_msg_env->mtype = KCD_REG;
	p_msg_env->mtext[0] = '%';
	p_msg_env->mtext[1] = 'z';
	p_msg_env->mtext[2] = 'R';
	p_msg_env->mtext[3] = '\0';

	send_message(2, (void*) p_msg_env);
	env = receive_message(sender_id_ptr);
	if (env != NULL && *sender_id_ptr == 2 && ((MSG_BUF*)env)->mtype == KCD_REG &&
		((MSG_BUF*)env)->mtext[0] == '%' && ((MSG_BUF*)env)->mtext[1] == 'z' &&
	  ((MSG_BUF*)env)->mtext[2] == 'R' && ((MSG_BUF*)env)->mtext[3] == '\0') {
		uart1_put_string("G005_test: test 9 OK\r\n"); //test7: expect receives 2nd message properly
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 9 FAIL\n\r");
	}
	if (release_memory_block(env) == RTX_OK) {
		uart1_put_string("G005_test: test 10 OK\r\n"); //test8: expect releases memory block OK
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 10 FAIL\n\r");
	}
	printf("G005_test: %d/%d tests OK\n\r", ok_counter, total_test);
	printf("G005_test: %d/%d tests FAIL\n\r", total_test - ok_counter, total_test);
	printf("G005_test: END\n\r");
	while ( 1 ) {
		release_processor();
	}
}

void proc3(void)
{
    
    while(1) {
        uart1_put_string("proc3: \n\r");
        release_processor();
    }
}

void proc4(void)
{
    while(1) {
        uart1_put_string("proc4: \n\r");
        release_processor();
    }
}

void proc5(void)
{
    while(1) {
        uart1_put_string("proc5: \n\r");
        release_processor();
    }
}

void proc6(void)
{
    while(1) {
        uart1_put_string("proc6: \n\r");
        release_processor();
    }
}
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
