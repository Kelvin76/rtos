/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTX LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *                          All rights reserved.
 *---------------------------------------------------------------------------
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice and the following disclaimer.
 *
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
 

/**************************************************************************//**
 * @file        k_process.h
 * @brief       process management hearder file 
 *              
 * @version     V1.2021.01
 * @authors     Yiqing Huang and Thomas Reidemeister
 * @date        2021 JAN
 * @note        Assuming there are only two user processes in the system
 *****************************************************************************/

#ifndef K_PROCESS_H_
#define K_PROCESS_H_

#include "k_inc.h"
#include "queue.h"
#include "null_process.h"
#include "timer.h"
#include "k_msg.h"
#include "kcd_process.h"
#include "crt_process.h"
#include "set_process_priority_process.h"
#include "process_A.h"
#include "process_B.h"
#include "process_C.h"
#include "wall_timer_process.h"
/*
 *===========================================================================
 *                             MACROS
 *===========================================================================
 */

#define INITIAL_xPSR 0x01000000			/* user process initial xPSR value */

#define NUM_PRIORITIES (PRI_NULL + 1)	/* Total number of priorities, assume PRI_NULL is the last one */

#define DEFAULT_PRIORITY LOWEST			/* Default priority is Lowest */				
/*
*==========================================================================
*                            GLOBAL VARIABLES
*==========================================================================
*/
extern bool g_is_released;				/* Indicates whether the processor is volunteerly released */

extern PCB** gp_pcbs;					/* array of pcbs */
extern PCB* gp_current_process;			/* always point to the current RUN process */

/* process initialization table (only for initialization) */
extern PROC_INIT g_proc_table[NUM_PROCS];  /* Test Procs + Null Proc */

/* scheduler (Priority queue) initialization (only new and rdy procs) */
extern queue g_scheduler[NUM_PRIORITIES];
/* a queue for the PCBs that are blocked on memory */
extern queue g_blked_mem_queue[NUM_PRIORITIES];
/* global timer counter */
extern volatile uint32_t g_timer_count;
/*
 *===========================================================================
 *                            FUNCTION PROTOTYPES
 *===========================================================================
 */

extern void process_init(PROC_INIT *proc_info, int num);        /* initialize all procs in the system */
extern PCB  *scheduler(void);                                   /* pick the pcb of the next to run process */
extern int  process_switch(void);
extern int  k_release_processor(void);                          /* kernel release_process function */
extern int  k_set_process_priority(int pid, int priority);      /* kernel set_process function */
extern int  k_get_process_priority(int pid);                    /* kernel get_process function */
extern int  k_run_new_process(void);                            /* kernel to schedule a process to run */

extern int  k_set_process_state(int pid, PCB* pcb, int state);			/* kernel (helper function) set the state of the process */
#endif /* ! K_PROCESS_H_ */
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
