#include <LPC17xx.h>

#include "crt_process.h"
#include "uart_polling.h"
#include "rtx.h"
#include "k_inc.h"

/**************************************************************************//**
 * @brief  The CRT display process is a privileged process. It responds to 
 * only one message type: a CRT display request.
 *****************************************************************************/

void	crt_proc(void) {
#ifdef DEBUG_0
	printf("==================================\r\n");
	printf("[DEBUG_0] CRT Proc () is called. \r\n");
#endif
	int send_id;
  MSG_BUF* p_msg;
	

	while (TRUE) {
		
		// block on receiving
		p_msg = receive_message(&send_id);
		
		if (p_msg->mtype == CRT_DISPLAY) {
			char* c_ptr = p_msg->mtext;
			int i = MAX_CMD_SIZE;
			int stringWillEnd = 0;
			while (!stringWillEnd && i > 0) {
				if (*c_ptr == '\0') {
					stringWillEnd = 1;
				}
				// uart1_put_char(*c_ptr);
				c_ptr++;
				i--;
			}
			if (!stringWillEnd) {
				uart1_put_string("[ERROR] CRT receives string with no end character.\n\r");
				if (RTX_ERR == release_memory_block((void *)p_msg)){
                uart1_put_string("[ERROR] CRT process releasing memory block failed.\n\r");
			  }
			} else {
			  if (RTX_ERR == send_message(PID_UART_IPROC, (void *)p_msg)){
          uart1_put_string("[ERROR] CRT process forwarding message to uart_i_proc failed.\n\r");
					if (RTX_ERR == release_memory_block((void *)p_msg)){
            uart1_put_string("[ERROR] CRT process releasing memory block failed.\n\r");
					}
			  } else {
				  /* Enable THRE, RBR left as enabled */
				  g_send_char++;
				  LPC_UART_TypeDef *pUart = (LPC_UART_TypeDef *) LPC_UART0;
			  	pUart->IER = IER_THRE | IER_RLS | IER_RBR;
		  	}
		  }
		} else  {
			if (RTX_ERR == release_memory_block((void *)p_msg)){
                uart1_put_string("[ERROR] CRT process releasing memory block failed.\n\r");
			}
		} 
	}
}
