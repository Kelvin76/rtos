#ifndef K_CRT_PROCESS_H_
#define K_CRT_PROCESS_H_

#include "common.h"
#include "uart_irq.h"

// define the CRT process
extern void crt_proc(void);	/* the CRT process */

#endif
