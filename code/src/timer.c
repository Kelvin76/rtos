/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTX LAB  
 *
 *  Copyright 2020-2021 NXP Semiconductors, Thomas Reidemeister and Yiqing Huang
 *                          All rights reserved.
 *---------------------------------------------------------------------------
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice and the following disclaimer.
 *
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
 

/**************************************************************************//**
 * @file        timer.c
 * @brief       timer.c - Timer example code. Timer IRQ is invoked every 1ms 
 *              
 * @version     V1.2021.02
 * @authors     NXP Semiconductors, Thomas Reidemeister and Yiqing Huang
 * @date        2021 FEB
 *****************************************************************************/
#include <LPC17xx.h>
#include "timer.h"
#include "k_rtx.h"
#include "k_inc.h"
#include "queue.h" 

#if defined(DEBUG_TIMER) || defined(DEBUG_TOTALWAIT) || defined(DEBUG_CURTIME)
#include "printf.h"
#include "uart_polling.h"
#endif

 /*
 *==========================================================================
 *                            GLOBAL VARIABLES
 *==========================================================================
 */

volatile uint32_t g_timer_count = 0;    // increment every 1 ms
volatile uint32_t g_totalWait = 0;      // decrement every 1 ms
queue    g_timeoutQ;                    // timeout queue for the delayed send messages

 /*
 *===========================================================================
 *                            FUNCTION PROTOTYPES
 *===========================================================================
 */

/**
 * @brief: initialize timer. Only timer 0 is supported
 */
uint32_t timer_init(uint8_t n_timer)
{
    LPC_TIM_TypeDef* pTimer;
    if (n_timer == 0) {
        /*
        Steps 1 & 2: system control configuration.
        Under CMSIS, system_LPC17xx.c does these two steps

        -----------------------------------------------------
        Step 1: Power control configuration.
                See table 46 pg63 in LPC17xx_UM
        -----------------------------------------------------
        Enable UART0 power, this is the default setting
        done in system_LPC17xx.c under CMSIS.
        Enclose the code for your refrence
        //LPC_SC->PCONP |= BIT(1);

        -----------------------------------------------------
        Step2: Select the clock source,
               default PCLK=CCLK/4 , where CCLK = 100MHZ.
               See tables 40 & 42 on pg56-57 in LPC17xx_UM.
        -----------------------------------------------------
        Check the PLL0 configuration to see how XTAL=12.0MHZ
        gets to CCLK=100MHZ in system_LPC17xx.c file.
        PCLK = CCLK/4, default setting in system_LPC17xx.c.
        Enclose the code for your reference
        //LPC_SC->PCLKSEL0 &= ~(BIT(3)|BIT(2));
        -----------------------------------------------------
        Step 3: Pin Ctrl Block configuration.
                Optional, not used in this example
                See Table 82 on pg110 in LPC17xx_UM
        -----------------------------------------------------
        */
        pTimer = (LPC_TIM_TypeDef*)LPC_TIM0;

    }
    else { /* other timer not supported yet */
        return 1;
    }

    /*
    -----------------------------------------------------
    Step 4: Interrupts configuration
    -----------------------------------------------------
    */

    /* Step 4.1: Prescale Register PR setting
       CCLK = 100 MHZ, PCLK = CCLK/4 = 25 MHZ
       2*(12499 + 1)*(1/25) * 10^(-6) s = 10^(-3) s = 1 ms
       TC (Timer Counter) toggles b/w 0 and 1 every 12500 PCLKs
       see MR setting below
    */
    pTimer->PR = 12499;

    /* Step 4.2: MR setting, see section 21.6.7 on pg496 of LPC17xx_UM. */
    pTimer->MR0 = 1;

    /* Step 4.3: MCR setting, see table 429 on pg496 of LPC17xx_UM.
       Interrupt on MR0: when MR0 mathches the value in the TC,
                         generate an interrupt.
       Reset on MR0: Reset TC if MR0 mathches it.
    */
    pTimer->MCR = BIT(0) | BIT(1);

    g_timer_count = 0;

    /* Step 4.4: CSMSIS enable timer0 IRQ */
    NVIC_EnableIRQ(TIMER0_IRQn);

    /* Step 4.5: Enable the TCR. See table 427 on pg494 of LPC17xx_UM. */
    pTimer->TCR = 1;

    return 0;
}

/**
 * @brief: use CMSIS ISR for TIMER0 IRQ Handler
 * NOTE: This example shows how to save/restore all registers rather than just
 *       those backed up by the exception stack frame. We add extra
 *       push and pop instructions in the assembly routine.
 *       The actual c_TIMER0_IRQHandler does the rest of irq handling
 */
__asm void TIMER0_IRQHandler(void)
{
				PRESERVE8
        IMPORT  timer0_iproc
        IMPORT  k_run_new_process
        CPSID   I                               // disable interrupt
        PUSH{ r4 - r11, lr }                    // save all registers
ITIMER_SAVE
        // save the sp of the current running process into its PCB
        LDR     R1, =__cpp(&gp_current_process) // load R1 with &gp_current_process
        LDR     R2, [R1]                        // load R2 with gp_current_process value
        STR     SP, [R2, #PCB_MSP_OFFSET]       // save MSP to gp_current_process->mp_sp

        // save the interrupted process's PCB in gp_pcb_interrupted
        LDR     R3, =__cpp(&gp_pcb_interrupted) // load &gp_pcb_interrupted to R3
        STR     R2, [R3]                        // assign gp_current_process to gp_pcb_interrupted

        // update gp_current_process to point to the gp_pcb_timer_iproc
        LDR     R1, =__cpp(&gp_pcb_timer_iproc) // load R1 with &gp_pcb_timer_iproc
        LDR     R2, [R1]                        // load R2 with gp_pcb_timer_iproc value
				LDR     R3, =__cpp(&gp_current_process) // load R3 with &gp_current_process
        STR     R2, [R3]                        // assign gp_pcb_timer_iproc to gp_current_process

ITIMER_EXEC
        // update gp_current_process to the PCB of timer_i_proc 
        LDR     R1, =__cpp(&gp_pcb_timer_iproc) // load R1 with &gp_pcb_timer_iproc 
        LDR     R2, [R1]                        // load R2 with gp_pcb_timer_iproc value
        LDR     SP, [R2, #PCB_MSP_OFFSET]       // load MSP with TIMER_IPROC's SP (i.e. gp_pcb_timer_iproc->mp_sp)
        BL      timer0_iproc                    // execute the timer i-process

ITIMER_RESTORE
        // updae the gp_current_process to gp_pcb_interrupted
        LDR     R1, =__cpp(&gp_pcb_interrupted) // load R1 with &gp_pcb_interrupted
        LDR     R2, [R1]                        // load R2 with gp_pcb_interrupted value
        LDR     R3, =__cpp(&gp_current_process) // load R3 with &gp_current_process
        STR     R2, [R3]                        // assign gp_pcb_interrupted to gp_current_process

        // restore the interrupted process's PCB to gp_current_process
        LDR     R1, =__cpp(&gp_pcb_interrupted)
        LDR     R2, [R1]
        LDR     SP, [R2, #PCB_MSP_OFFSET]       // load MSP with gp_current_process->mp_sp
        BL      k_run_new_process               // run a Non-IPROC 

        CPSIE   I                               // enable interrupt
        POP{ r4 - r11, pc }                     // restore all registers
}

 /**************************************************************************//**
  * @brief   c TIMER0 IRQ Handler is now a timer i-process with its own PCB and stack
  *          Handles time increment and delayed send messages
  * @post    calling scheduler to decide which process to run next
  *****************************************************************************/
void timer0_iproc(void)
{

#ifdef DEBUG_TIMER
		//uart1_put_string("[Timer0 Start] Enter timer0, waiting for end.\n\r");
		if(gp_pcb_timer_iproc->m_pid != gp_current_process->m_pid){			
				printf("timer: %x.  current: %x.\n\r",&gp_pcb_timer_iproc, &gp_current_process);
				printf("timer id: %d.  current id: %d.\n\r",gp_pcb_timer_iproc->m_pid, gp_current_process->m_pid);
		}
#endif /* DEBUG_TIMER */
	
    /* ack inttrupt, see section  21.6.1 on pg 493 of LPC17XX_UM */
    LPC_TIM0->IR = BIT(0);

    // initialize some local variables
    MSG_BUF* msg;
    int target_pid;
		int sender_id;

    // increment current time
    g_timer_count++;
    // decrement the total wait time in the timeoutQ and the timeout value of the first element
    if (g_timeoutQ.size) {
        g_totalWait--;
        ((MSG_BUF*)g_timeoutQ.head)->m_kdata[1]--;
			
				#ifdef DEBUG_TOTALWAIT		
				if(g_totalWait > 1000){
						printf("timeoutQ.size: %d.\n\r", g_timeoutQ.size);
				    printf("totalWait: %d.\n\r", g_totalWait);
				    printf("head_timeout: %d.\n\r", ((MSG_BUF*)g_timeoutQ.head)->m_kdata[1]);
				}

				if(g_totalWait%50 == 0 && g_totalWait < 1000){
				    printf("TotalWait: %d.\n\r", g_totalWait);
				}
				#endif /* DEBUG_TOTALWAIT */
		}
		#ifdef DEBUG_TOTALWAIT			
		else{
						//printf("timeoutQ.size: %d.\n\r", g_timeoutQ.size);
		}
		#endif /* DEBUG_TOTALWAIT */
		
    /* get pending requests */
    while (gp_current_process->msg_queue.size > 0) {
			
				#ifdef DEBUG_TIMER
				printf("\n\r  MsgQ size: %d.\n\r", gp_current_process->msg_queue.size);
				#endif /* DEBUG_TIMER */
			
        msg = k_receive_message(&sender_id);
        // insert envelope into the timeout queue
				
			  // if the message should have been sent already
				if(msg->m_kdata[1] < 0){
						// add it as the head of the queue
						if (RTX_ERR == enqueue_opposite(&g_timeoutQ, (node*)msg)){
								#if defined(DEBUG_TIMER) || defined(DEBUG_0) || defined(DEBUG_TOTALWAIT)
								uart1_put_string("[Timer0 Error] Enqueue to g_timerQ failed.\n\r");
								#endif /* DEBUG_TIMER */	
                return;
						}
					
						#if defined(DEBUG_TOTALWAIT)
						printf("\n\r     Sender: %d. timeout: %d.\n\r", sender_id, msg->m_kdata[1]);
						#endif /* DEBUG_TOTALWAIT */
				}
        // if the delay is greater than the delay of the whole queue, add it at the back
        else if (msg->m_kdata[1] >= g_totalWait) {
            // modify the timeout value of the message and enqueue it to the timeoutQ
            msg->m_kdata[1] -= g_totalWait;
            if (RTX_ERR == enqueue(&g_timeoutQ, (node*)msg)) {
								#if defined(DEBUG_TIMER) || defined(DEBUG_0) || defined(DEBUG_TOTALWAIT)
								uart1_put_string("[Timer0 Error] Enqueue to g_timerQ failed.\n\r");
								#endif /* DEBUG_TIMER */						
                return;
            }
						// update the total wait, simply add the timeout value of the msg at this moment
						g_totalWait += msg->m_kdata[1];
        }
        // The message should be inserted in the middle of the queue
        else {
            MSG_BUF* tmpNode = (MSG_BUF*)g_timeoutQ.head;
            // if the message is earlier than the first message in the queue
            if (msg->m_kdata[1] < tmpNode->m_kdata[1]) {
							// add the msg as the head of the queue
							if (RTX_ERR == enqueue_opposite(&g_timeoutQ, (node*)msg)){
									#if defined(DEBUG_TIMER) || defined(DEBUG_0) || defined(DEBUG_TOTALWAIT)
									uart1_put_string("[Timer0 Error] Enqueue to g_timerQ failed.\n\r");
									#endif /* DEBUG_TIMER */	
									return;
							}
            }
            else {
								// reduce the delay by the timeout of the head
                msg->m_kdata[1] -= ((MSG_BUF*)g_timeoutQ.head)->m_kdata[1];
                // if the delay of the message is later than the next message, we proceed to the next node
                while (msg->m_kdata[1] > ((MSG_BUF*)tmpNode->mp_next)->m_kdata[1]) {
                    msg->m_kdata[1] -= ((MSG_BUF*)tmpNode->mp_next)->m_kdata[1];
                    tmpNode = tmpNode->mp_next;
                }
                // found the place to put the message
                // modify the queue
                msg->mp_next = tmpNode->mp_next;
                tmpNode->mp_next = msg;
								
								g_timeoutQ.size++;
            }

            // update the timeout value for messages after the new message in the timeout queue
            if(msg->mp_next != NULL){
							((MSG_BUF*)msg->mp_next)->m_kdata[1] -= msg->m_kdata[1];
						}
        }

    }

    // if the timeoutQ is not empty and the first env is about to expire
    while (g_timeoutQ.size && ((MSG_BUF*)g_timeoutQ.head)->m_kdata[1] <= 0) {
        msg = (MSG_BUF*)dequeue(&g_timeoutQ);

        /* forward msg to destination */
        target_pid = msg->m_recv_pid;
		
		#ifdef DEBUG_TIMER
				printf("pid: %d.\n\r", target_pid);
		#endif /* DEBUG_TIMER */
			
			
        k_send_message(target_pid, msg);
    }
	
#ifdef DEBUG_TIMER
		//uart1_put_string("[Timer0 End] Leaving timer0.\n\r");
#endif /* DEBUG_TIMER */
}


/*
Note that the structure of MSG_BUF is
    
    m_kdata[0] = Timer_i_pid
    m_kdata[1] = # of clock ticks


    typedef struct msgbuf
    {
    #ifdef K_MSG_ENV
        void* mp_next;              
        int m_send_pid;           
        int m_recv_pid;            
        int m_kdata[5];            
    #endif
        int mtype;                
        char mtext[1];           
    } MSG_BUF;

*/




 /**************************************************************************//**
  * @brief   get current time since the beginning of the system
  * @return  g_timer_count
  *****************************************************************************/

int k_get_current_time(void) {
	
		#ifdef DEBUG_CURTIME
		printf("Current time: %d.\n\r", g_timer_count);
		#endif /* DEBUG_TIMER */
	
    return g_timer_count;
}


//TODO: could add a helper function to print the queue for testing

/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
