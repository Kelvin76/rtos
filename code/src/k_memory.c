/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTX LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *                          All rights reserved.
 *---------------------------------------------------------------------------
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice and the following disclaimer.
 *
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
 

/**************************************************************************//**
 * @file        k_memory.c
 * @brief       kernel memory managment routines starter source code 
 *              
 * @version     V1.2021.01
 * @authors     Yiqing Huang
 * @date        2021 JAN
 * @attention   only two processes are initialized, not all six user test procs
 *****************************************************************************/
#include "k_memory.h"
#include "k_process.h"
#include "k_rtx.h"
#include "queue.h"

/*
 *==========================================================================
 *                            GLOBAL VARIABLES
 *==========================================================================
 */

U32 *gp_stack; /* The last allocated stack low address. 8 bytes aligned */
               /* The first stack starts at the RAM high address        */
	           /* stack grows down. Fully decremental stack             */
queue mem_pool = {.size=0, .head = NULL, .tail = NULL};
queue* mem_pool_ptr = &mem_pool;
const int my_magic_word = 12345;
/**************************************************************************//**
 * @brief: Initialize RAM as follows:

0x10008000+---------------------------+ High Address
          |    Proc 1 STACK           |
          |---------------------------|
          |    Proc 2 STACK           |
          |---------------------------|<--- gp_stack
          |                           |
          |        HEAP               |
          |                           |
          |---------------------------|
          |        PCB 2              |
          |---------------------------|
          |        PCB 1              |
          |---------------------------|
          |        PCB pointers       |
          |---------------------------|<--- gp_pcbs
          |        Padding            |
          |---------------------------|  
          |Image$$RW_IRAM1$$ZI$$Limit |
          |...........................|          
          |       RTX  Image          |
          |                           |
0x10000000+---------------------------+ Low Address

*/
/**************************************************************************//**
 * @brief   	initialize the memory
 * @details     allocating memory for PCBs
 *              initial stack base pointer for processes
 *****************************************************************************/
 
 
void memory_init(void)
{
	//todo: why U8 here?
	U8 *p_end = (U8 *)&Image$$RW_IRAM1$$ZI$$Limit;
	int i;
  
	/* 4 bytes padding */
	p_end += 4;

	/* allocate memory for pcb pointers   */
	gp_pcbs = (PCB **)p_end;
	p_end += NUM_PROCS * sizeof(PCB *);
  #ifdef DEBUG_MEM  
		printf("gp_pcbs = 0x%x \r\n", gp_pcbs);
	#endif
	for ( i = 0; i < NUM_PROCS; i++ ) {
		gp_pcbs[i] = (PCB *)p_end;
		p_end += sizeof(PCB); 
	}
#ifdef DEBUG_MEM  
	for ( i = 0; i < NUM_PROCS; i++ ) {
		printf("gp_pcbs[%02d] = 0x%x \r\n",i, gp_pcbs[i]);
	}
#endif
	
	/* prepare for alloc_stack() to allocate memory for stacks */
	
	gp_stack = (U32 *)RAM_END_ADDR;
	if ((U32)gp_stack & 0x04) { /* 8 bytes alignment */
		--gp_stack; 
	}
  
	#ifdef DEBUG_MEM
		printf("heap start at = 0x%x \r\n", p_end);
	#endif
	/* allocate memory for heap*/
	for (int i = 0; i < MEM_NUM_BLKS; i++) {
		//byte addressable, so use U8
		//start from end of pcb, to beginning of stack
		U8* addr = p_end + i*MEM_BLK_SIZE;
		if ((addr + MEM_BLK_SIZE) <= (U8*)gp_stack) {

			mem_blk blk_i = {.next = NULL, .pid = NULL, .magic_word = my_magic_word};//todo: what should the init pid be
			*(mem_blk*) addr = blk_i;
			#ifdef DEBUG_MEM
				printf("==================================\r\n");
				printf("memory block %d allocated\r\n", i);
				printf("address = 0x%x \r\n", addr);
				printf("magic word = %d \r\n", ((mem_blk*)addr)->magic_word);
			#endif
			enqueue(mem_pool_ptr, (node*)addr);
		}
	}
	#ifdef DEBUG_MEM
		printf("Now iterating over queue\r\n");
		node* a = mem_pool.head;
		printf("Queue size %d\r\n", mem_pool.size);
		while (a != NULL) {
			printf("==================================\r\n");
			printf("q node 0x%x \r\n", a);
			printf("q node pointing to 0x%x \r\n", a->next);
			printf("magic word = %d \r\n", ((mem_blk*)a)->magic_word);
			a = a->next;
		}
	#endif
}

/**************************************************************************//**
 * @brief   	allocate stack for a process, align to 8 bytes boundary
 * @param       size stack size in bytes
 * @return      the base of the stack (i.e. high address)
 * @post        gp_stack is updated.
 *****************************************************************************/

U32 *alloc_stack(U32 size_b) 
{
	U32 *sp;
	sp = gp_stack; /* gp_stack is always 8 bytes aligned */
	
	/* update gp_stack */
	gp_stack = (U32 *)((U8 *)sp - size_b);
	
	/* 8 bytes alignement adjustment to exception stack frame */
	if ((U32)gp_stack & 0x04) {
		--gp_stack; 
	}
	return sp;
}
/*
 *==========================================================================
 *                            TO BE IMPLEMENTED
 *==========================================================================
 */
void *k_request_memory_block_nb() {
#ifdef DEBUG_MEM 
	printf("k_request_memory_block_nb: entering...\n");
#endif /* ! DEBUG_MEM */
	if (gp_current_process != NULL && gp_current_process->m_mem_given != NULL) {
		#ifdef DEBUG_MEM 
			printf("k_request_memory_block_nb: memory is already assigned to block, returning\n");
		#endif /* ! DEBUG_MEM */
		//if we have given a mem to it in release_memory
		//do not have to set pid here
		//already set that in release_memory
		void* tmp_ptr = gp_current_process->m_mem_given;
		gp_current_process->m_mem_given = NULL;
		return tmp_ptr;
	}
	void* ret = dequeue(mem_pool_ptr);
	if (ret != NULL) {
		((mem_blk*)ret)->pid = gp_current_process->m_pid;
		//cast memory block to memory
		//return memory starts from after the member variables
		ret = (U8*)ret + sizeof(mem_blk);
		#ifdef DEBUG_MEM 
			printf("k_request_memory_block_nb: have valid memory, returning 0x%x\n", ret);
		#endif /* ! DEBUG_MEM */
	}
	return (void*)ret;
}
void *k_request_memory_block(void) {
#if defined(DEBUG_0) || defined(DEBUG_MEM)
	printf("k_request_memory_block: entering...\n");
#endif /* ! DEBUG_0 */
	void* mem_ptr = k_request_memory_block_nb();
	while (mem_ptr == NULL) {
		#ifdef DEBUG_MEM 
			printf("k_request_memory_block: blocked by memory\n");
		#endif /* ! DEBUG_MEM */
		//put PCB on blocked memory queue and set pcb state
		k_set_process_state(gp_current_process->m_pid, gp_current_process, BLK_MEM);
		k_release_processor();
		//process back from stuck state, needd to check memory again
		mem_ptr = k_request_memory_block_nb();
	}
	return (void *) mem_ptr;
}

int k_validate_mem_blk(void* p_mem_blk) {
	if (p_mem_blk == NULL || //check null
		(U8*)p_mem_blk > (U8*)gp_stack - MEM_BLK_SIZE || //check if greater than range
		(U8*)p_mem_blk < (U8*)gp_pcbs[NUM_TEST_PROCS - 1] + sizeof(PCB) || //check smaller than range
		((mem_blk*)p_mem_blk)->magic_word != my_magic_word || // check data integrity
		gp_current_process->m_pid != ((mem_blk*)p_mem_blk)->pid) /*check PID*/ {
		return RTX_ERR;
	}
	return RTX_OK;
}

int k_release_memory_block(void* p_mem_blk) {
#if defined(DEBUG_0) || defined(DEBUG_MEM) 
	printf("k_release_memory_block: releasing block @ 0x%x\n", p_mem_blk);
#endif /* ! DEBUG_0 */
	//check if memblock is invalid - todo:how?
	if (p_mem_blk != NULL) {
		p_mem_blk = (U8*)p_mem_blk - sizeof(mem_blk); //get the address of the actual mem block
	}
	if (k_validate_mem_blk(p_mem_blk) != RTX_OK) {
		#ifdef DEBUG_MEM 
			printf("k_release_memory_block: memory invalid...\n");
		#endif /* ! DEBUG_MEM */
		return RTX_ERR;
	}
	//check blocked on memory queue
	for (int priority_to_check = HIGH; priority_to_check < PRI_NULL; priority_to_check++) { //null process will never be blocked, only check 123
		PCB* my_pcb_ptr = (PCB*)g_blked_mem_queue[priority_to_check].head;
		if (my_pcb_ptr != NULL) {
			#ifdef DEBUG_MEM
				printf("k_release_memory_block: giving memory to process...\n");
			#endif /* ! DEBUG_MEM */
			((mem_blk*)p_mem_blk)->pid = my_pcb_ptr->m_pid;
			my_pcb_ptr->m_mem_given = (U8*)p_mem_blk + sizeof(mem_blk);
			//if priority of my_pcb is higher, this will trigger context switch
			k_set_process_state(my_pcb_ptr->m_pid, my_pcb_ptr, RDY); 
			return RTX_OK;
		}
	}

	//no blocked on memory queue
	#ifdef DEBUG_MEM 
	printf("k_release_memory_block: No process blocked, enqueuing...\n");
	#endif /* ! DEBUG_MEM */
	enqueue(mem_pool_ptr, (node*)p_mem_blk);

	return RTX_OK;
}

int k_transfer_ownership(void* p_msg_blk, int p_id){
	#if defined(DEBUG_0) || defined(DEBUG_MEM) || defined(DEBUG_MSG)
		printf("k_transfer_ownership: from current process %02d to process %02d\n", gp_current_process->m_pid, p_id);
	#endif
	if (p_msg_blk != NULL) {
		void* p_mem_blk = (U8*)p_msg_blk - sizeof(mem_blk); //get the address of the actual mem block
		((mem_blk*)p_mem_blk)->pid = p_id;
		return RTX_OK;
	}
	return RTX_ERR;
}

/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
