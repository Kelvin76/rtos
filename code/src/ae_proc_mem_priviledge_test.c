/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTOS LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *
 *          This software is subject to an open source license and 
 *          may be freely redistributed under the terms of MIT License.
 ****************************************************************************
 */

/**************************************************************************//**
 * @file        ae_proc_mem_priviledge_test.c
 * @brief       test memory api and previledge issues
 *              
 * @version     V1.2021.02
 * @authors     JL
 * @date        2021 Feb
 * @note        Each process is in an infinite loop. Processes never terminate.
 * @details
 *
 *****************************************************************************/

#include "rtx.h"
#include "uart_polling.h"
#include "ae_proc.h"
#include "printf.h"
#include <LPC17xx.h>

int ok_counter = 0;
int total_test = 3;
/**************************************************************************//**
 * @brief: a process that prints two lines of five uppercase letters
 *         and then changes P2's priority to HIGH
 *         and then yields the cpu.
 *****************************************************************************/
void proc1(void)
{	
	uart1_put_string("G005_test: START\r\n"); 
	printf("G005_test: total %d tests\r\n", total_test); 
	int myctrl = __get_CONTROL();
	if (myctrl == 1) {//expect unpriviledged
		uart1_put_string("G005_test: test 1 OK\r\n");
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 1 FAIL\n\r");
	}
	set_process_priority(PID_P1, LOWEST);
	while ( 1 ) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief  a process that prints five numbers, change P1's priority to HIGH
 *         and then yields the cpu.
 *****************************************************************************/
void proc2(void)
{
	int ret_val = 20;
	void *p_mem_blk;
	p_mem_blk = request_memory_block();
	p_mem_blk = (U8*) p_mem_blk + 4;
	ret_val = release_memory_block(p_mem_blk);
	if (ret_val == -1) { //expect chec data integrity to fail
		uart1_put_string("G005_test: test 2 OK\r\n"); 
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 2 FAIL\n\r");
	}
	
	p_mem_blk = (U8*) p_mem_blk - 4;
	ret_val = release_memory_block(p_mem_blk);
	if (ret_val == 0) { //expect release success
		uart1_put_string("G005_test: test 3 OK\r\n"); 
		ok_counter++;
	} else {
		uart1_put_string("G005_test: test 3 FAIL\n\r");
	}
	
	printf("G005_test: %d/%d test OK\n\r", ok_counter, total_test);
	printf("G005_test: %d/%d test FAIL\n\r", total_test - ok_counter, total_test);
	printf("G005_test: END\n\r");

	while ( 1 ) {
		release_processor();
	}
}

void proc3(void)
{
    
    while(1) {
        uart1_put_string("proc3: \n\r");
        release_processor();
    }
}

void proc4(void)
{
    while(1) {
        uart1_put_string("proc4: \n\r");
        release_processor();
    }
}

void proc5(void)
{
    while(1) {
        uart1_put_string("proc5: \n\r");
        release_processor();
    }
}

void proc6(void)
{
    while(1) {
        uart1_put_string("proc6: \n\r");
        release_processor();
    }
}
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
