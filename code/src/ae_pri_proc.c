/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTOS LAB
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *
 *          This software is subject to an open source license and
 *          may be freely redistributed under the terms of MIT License.
 ****************************************************************************
 */

 /**************************************************************************//**
  * @file        ae_pri_proc.c
  * @brief       Two auto test processes: P1-P5
  *
  * @version     V1.2021.02
  * @authors     KW
  * @date        2021 Feb
  * @note        Each process is in an infinite loop. Processes never terminate.
  *	             Most importantly, this is only used when there is no TIMER_IPROC. 
  *              I.E., if timer is added, the sequence of the tests cases might change.
  * @details
  *
  *****************************************************************************/
  /*----------------------------------------------------------------------------
   * Several test cases:
   * 1. Set current process from priority HIGH to MEDIUM while existing other ready process with priority HIGH.
   *    Expected result: switch to the other ready process with priority HIGH.
   * 2. Set current process from priority HIGH to MEDIUM while no other ready process is in priority HIGH.
   *    Expected result: continue with current process.
   * 3. Set other ready process to priority HIGH while current process has priority MEDIUM.
   *    Expected result: switch to the other ready process with priority HIGH.
   * 4. Set other ready process to priority HIGH while current process has priority HIGH.
   *    Expected result: continue with current process.

   * Expected COM1 Output:

   * G005_test: START
   * G005_test: total 16 tests
   * G005_test: test 1 OK
   * G005_test: test 2 OK
   * G005_test: test 3 OK
   * G005_test: test 4 OK
   * G005_test: test 5 OK.
   * G005_test: test 6 OK.
   * G005_test: test 7 OK.
   * G005_test: test 8 OK.
   * G005_test: test 9 OK.
   * G005_test: test 10 OK.
   * G005_test: test 11 OK.
   * G005_test: test 12 OK.
   * G005_test: test 13 OK.
   * G005_test: test 14 OK.
   * G005_test: test 15 OK.
   * G005_test: test 16 OK.
   * G005_test: 16/16 tests OK
   * G005_test: 0/16 tests FAIL
   * G005_test: END
   *---------------------------------------------------------------------------*/
#include "rtx.h"
#include "uart_polling.h"
#include "ae_proc.h"
#include "printf.h"


#define _AE_PRI_TOTAL_TESTS_ 16
int passed_tests = 0;

/**************************************************************************//**
 * @brief: a process that was originally with HIGH priority
 *		 set this process (current process) to MEDIUM while exists other
 *		 process with priority HIGH. Should cause preemption and switch the process.
 *****************************************************************************/
void proc1(void)
{
#ifdef DEBUG_0
	printf("[Process 1] Start executing process 1 \n");
	printf("[Process 1] process 1 priority = %02d \n", get_process_priority(PID_P1));
#endif /* DEBUG_0 */

	int i = 0;
	int counter = 0;
	int ret_val = 100;

	// Start the test
	uart1_put_string("G005_test: START \n\r");
	printf("G005_test: total %d tests \n\r", _AE_PRI_TOTAL_TESTS_);

	// Test if invalid process id could return RTX_ERR
	ret_val = get_process_priority(-1);
	if (ret_val == -1) {
		uart1_put_string("G005_test: test 1 OK\n\r");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 1 FAIL\n\r");
	}

	while (1) {
		if (i != 0 && i % 5 == 0) {
			counter++;
			if (counter == 1) {
				// Set the second process to priority HIGH
				set_process_priority(PID_P2, HIGH);

				ret_val = get_process_priority(PID_P2);
				if (ret_val == HIGH) {
					uart1_put_string("G005_test: test 2 OK\n\r");
					passed_tests++;
				}
				else {
					uart1_put_string("G005_test: test 2 FAIL\n\r");
				}

#ifdef DEBUG_0
				printf("[Process 1] Set process 1 to MEDIUM \n");
#endif /* DEBUG_0 */

				// Then set this process to MEDIUM
				// Should cause preemption and gives the processor to P2
				ret_val = set_process_priority(PID_P1, MEDIUM);

#ifdef DEBUG_0
				printf("[Process 1] Back from process 2 \n", ret_val);
				printf("[Process 1] process 1 priority = %02d \n", get_process_priority(PID_P1));
				printf("[Process 1] process 2 priority = %02d \n", get_process_priority(PID_P2));
#endif /* DEBUG_0 */

				// Set the second process to priority HIGH again
				// Should continue running
				ret_val = set_process_priority(PID_P2, HIGH);
			}
			else {
				break;
			}
		}
		i++;
	}
	release_processor();
	// process 2 will give the processor to process 1 again
	uart1_put_string("G005_test: test 3 OK\n\r");
	passed_tests++;
	set_process_priority(PID_P1, LOWEST);
	while (1) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief  a process that was originally with HIGH priority
 *         Will set this process to HIGH and MEDIUM for a couple of times
 *****************************************************************************/
void proc2(void)
{

#ifdef DEBUG_0
	printf("[Process 2] Start executing process 2 \n");
	printf("[Process 2] process 2 priority = %02d \n", get_process_priority(PID_P2));
#endif /* DEBUG_0 */

	int i = 0;
	int counter = 0;

	while (1) {
		if (i != 0 && i % 5 == 0) {
			counter++;
			if (counter == 1) {
				// Set the current process to priority MEDIUM
				// Should continue to run
				set_process_priority(PID_P2, MEDIUM);
			}
			else if (counter >= 2) {
				break;
			}
		}
		i++;
	}


	// should switch to Process 1
#ifdef DEBUG_0
	printf("[Process 2] process 1 priority = %02d \n", get_process_priority(PID_P1));
	printf("[Process 2] process 2 priority = %02d \n", get_process_priority(PID_P2));
	printf("[Process 2] Set process 1 to HIGH priority \n");
#endif /* DEBUG_0 */

	// Set priority of process 1 to be HIGH
	// Should cause preemption and switch to process 1
	set_process_priority(PID_P1, HIGH);

#ifdef DEBUG_0
	printf("[Process 2] Back from process 1 \n");
#endif /* DEBUG_0 */

	// reset counter 
	counter = 0;
	i = 0;
	while (1) {
		if (i != 0 && i % 5 == 0) {
			counter++;
			if (counter == 1) {
				// Set the current process to priority MEDIUM
				// Should switch to process 1 since proc 1 is in HIGH priority
				// But process 1 will imediately set itself to LOWEST
				// So we are expecting process 2 to continue

#ifdef DEBUG_0
				printf("[Process 2] process 2 priority = %02d \n", get_process_priority(PID_P2));
				printf("[Process 2] Set process 2 to MEDIUM priority \n");
#endif /* DEBUG_0 */

				set_process_priority(PID_P2, MEDIUM);
			}
			else if (counter >= 2) {
				break;
			}
		}
		i++;
	}
	uart1_put_string("G005_test: test 4 OK\n\r");
	passed_tests++;

	set_process_priority(PID_P3, HIGH);
	while (1) {
		release_processor();
	}
}

void proc3(void)
{
	set_process_priority(PID_P2, LOWEST);
	// Test 1
	set_process_priority(PID_P1, MEDIUM);
	if (get_process_priority(PID_P1) == MEDIUM) {
		uart1_put_string("G005_test: test 5 OK.\n\r");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 5 FAIL.\n\r");
	}

	set_process_priority(PID_P6, LOWEST);
	if (get_process_priority(PID_P6) == LOWEST) {
		uart1_put_string("G005_test: test 6 OK.\n\r");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 6 FAIL.\n\r");
	}

	set_process_priority(PID_P3, HIGH);
	if (get_process_priority(PID_P3) == HIGH) {
		uart1_put_string("G005_test: test 7 OK.\n\r");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 7 FAIL.\n\r");
	}

	// Test 2
	if (set_process_priority(-1, MEDIUM) == RTX_ERR) {
		uart1_put_string("G005_test: test 8 OK.\n\r");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 8 FAIL.\n\r");
	}

	// Test 3
	if (set_process_priority(-1, -1) == RTX_ERR) {
		uart1_put_string("G005_test: test 9 OK.\n\r");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 9 FAIL.\n\r");
	}

	// Test 4
	if (set_process_priority(PID_P3, -1) == RTX_ERR) {
		uart1_put_string("G005_test: test 10 OK.\n\r");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 10 FAIL.\n\r");
	}

	// goto proc 4
	set_process_priority(PID_P4, HIGH);

	while (1) {
		release_processor();
	}
}

void proc4(void)
{
	// reset the priorities
	set_process_priority(PID_P1, LOWEST);
	set_process_priority(PID_P2, LOWEST);
	set_process_priority(PID_P3, LOWEST);
	set_process_priority(PID_P5, HIGH);
	set_process_priority(PID_P6, LOWEST);

	printf("G005_test: test 11 OK\n\r");
	passed_tests++;
	set_process_priority(PID_P4, MEDIUM);  // Goto (1)
	// should get preempted

	// should get back from process 5
	printf("G005_test: test 13 OK\n\r");  // (2)
	passed_tests++;
	set_process_priority(PID_P4, LOW);  // Goto (3)
	// should preempted to 5 again

	printf("G005_test: test 15 OK\n\r");  // (4)
	passed_tests++;

	set_process_priority(PID_P5, MEDIUM);
	set_process_priority(PID_P4, MEDIUM); // drop one level should opt to (5)

	// should never get here
	printf("G005_test: test 16 FAIL\n\r");

	// print out for tests
	printf("G005_test: %01d/%01d tests OK\n\r", passed_tests, _AE_PRI_TOTAL_TESTS_);
	printf("G005_test: %01d/%01d tests FAIL\n\r", _AE_PRI_TOTAL_TESTS_ - passed_tests, _AE_PRI_TOTAL_TESTS_);
	uart1_put_string("G005_test: END\n\r");
}

void proc5(void)
{
	printf("G005_test: test 12 OK\n\r"); // (1)
	passed_tests++;
	set_process_priority(PID_P4, HIGH);

	set_process_priority(PID_P5, MEDIUM); // Goto (2)
	// should get preempted
	printf("G005_test: test 14 OK\n\r");  // (3)
	passed_tests++;

	// should get preempted
	set_process_priority(PID_P4, HIGH); // Goto (4)
	// should get preempted to process 4

	// should get preempted back (5)
	printf("G005_test: test 16 OK\n\r");
	passed_tests++;

	// print out for tests
	printf("G005_test: %01d/%01d tests OK\n\r", passed_tests, _AE_PRI_TOTAL_TESTS_);
	printf("G005_test: %01d/%01d tests FAIL\n\r", _AE_PRI_TOTAL_TESTS_ - passed_tests, _AE_PRI_TOTAL_TESTS_);
	uart1_put_string("G005_test: END\n\r");
}

void proc6(void)
{
	while (1) {
		release_processor();
	}
}
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
