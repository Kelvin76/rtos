
#include "rtx.h"
#include "uart_polling.h"
#include "k_inc.h"
#include "string.h"
#include "wall_timer_process.h"

char clockMessage[]= "\x1b[s\x1b[1;1000f\x1b[7D00:00:00\r\n\x1b[u\0";

int toASCII(int x){
  return 	x+48;
}

int toInt(int x){
	int result = 0;
	result = x-48;
	if (result >=0 && result <=9) {
		return result;
	} else {
		return 0;
	}
}

void sendMessage(U32 timer, bool clockOn){

	if(clockOn){
		MSG_BUF* msg = (MSG_BUF*) request_memory_block();
	  msg->mtype = CRT_DISPLAY;
	#ifndef SIM_TARGET 
		// first index of 00:00:00 is 16
		str_cpy(msg->mtext,clockMessage);
		msg->mtext[16] = toASCII(timer /3600 / 10);
		msg->mtext[17] = toASCII((timer /3600) % 10);
		msg->mtext[19] = toASCII((timer %3600) /60 / 10);
		msg->mtext[20] = toASCII((timer %3600) /60 % 10);
		msg->mtext[22] = toASCII((timer %60) / 10);
		msg->mtext[23] = toASCII((timer %3600) % 10);
	//send msg for SIM
	#else 
		msg->mtext[0] = toASCII(timer /3600 / 10);
		msg->mtext[1] = toASCII((timer /3600) % 10);
		msg->mtext[2] = ':';
		msg->mtext[3] = toASCII((timer %3600) /60 / 10);
		msg->mtext[4] = toASCII((timer %3600) /60 % 10);
		msg->mtext[5] = ':';
		msg->mtext[6] = toASCII((timer %60) / 10);
		msg->mtext[7] = toASCII((timer %3600) % 10);
		msg->mtext[8] = '.';
		msg->mtext[9] = '\r';
		msg->mtext[10] = '\n';
		msg->mtext[11] = '\0';
	#endif
	send_message(PID_CRT, msg);
	}else{
  #ifndef SIM_TARGET 
	// clean the top right corner
	  MSG_BUF* msg = (MSG_BUF*) request_memory_block();
	  msg->mtype = CRT_DISPLAY;
		str_cpy(msg->mtext,clockMessage);
		msg->mtext[16] = ' ';
		msg->mtext[17] = ' ';
		msg->mtext[18] = ' ';
		msg->mtext[19] = ' ';
		msg->mtext[20] = ' ';
		msg->mtext[21] = ' ';
		msg->mtext[22] = ' ';
		msg->mtext[23] = ' ';
		send_message(PID_CRT, msg);
	//do nothing for SIM
	#endif	
	}
}

bool isInt (int x){
	return ((char)x >= '0' && (char)x <= '9')?true:false;
}

void wall_clock_process(){
	U32 timer = 0;
	U8 isRun = 0;
	bool clockOn = false;
  bool gogo = false;
	int sender_id = 0;
	
	MSG_BUF* command = request_memory_block();
	command->mtype = KCD_REG;
	str_cpy(command->mtext, "%W");
	
	// register the command
	send_message(PID_KCD, command);

	while (1) {
		command = receive_message(&sender_id);
		if (sender_id ==  PID_KCD){
			
			if((command->mtext[2] == 'R' || command->mtext[2] == 'T') && (command->mtext[3] != '\r')){
				gogo = false;
				command->mtype = CRT_DISPLAY;
				// got extra characters
				str_cpy(command->mtext, "Invalid command.\r\n\0");
				send_message(PID_CRT, command);
				continue;
			}
			
			// %WS 12:23:34
			if ((command->mtext[2] == 'S') || (command->mtext[2] == 'R')) {
					
				if(command->mtext[2] == 'R') {
					gogo = true;
					clockOn = true;
					isRun ++;
					timer = 0;
				
				} else {
					// input is non-number || input is invalid
					if (!(command->mtext[3] == ' ' && isInt(command->mtext[4]) && isInt(command->mtext[5])
						&& command->mtext[6] == ':' && isInt(command->mtext[7]) && isInt(command->mtext[8])
					&& (command->mtext[9] == ':') && isInt(command->mtext[10]) && isInt(command->mtext[11]) && command->mtext[12]=='\r' )){
						  
						// report non-number error
						gogo = false;
						command->mtype = CRT_DISPLAY;
						if(command->mtext[12]=='\r'){
							str_cpy(command->mtext, "Invalid Input: h/m/s should be integers.\r\n\0");
						}else{
							// got extra characters
							str_cpy(command->mtext, "Invalid format: [%WS hh:mm:ss]\r\n\0");
						}
						send_message(PID_CRT, command);
					} else { 

						//%WS 12:23:34
						int hours = toInt(command->mtext[4]) * 10 + toInt(command->mtext[5]);
						int minutes = toInt(command->mtext[7]) * 10 + toInt(command->mtext[8]);
						int seconds = toInt(command->mtext[10]) * 10 + toInt(command->mtext[11]);
						
						// time is invalid time must <= 23:59:59
						if (hours <0 || hours>23 || minutes <0 || minutes > 59 || seconds <0 || seconds>59) {
								// report invalid number
							  gogo = false;
								command->mtype = CRT_DISPLAY;
							  str_cpy(command->mtext, "Invalid time. hh:mm:ss <= 23:59:59.\r\n\0");
								send_message(PID_CRT, command);
						} else {
								gogo = true;
								clockOn = true;
								isRun ++;
								timer = (3600 * hours + 60 * minutes + seconds) % (60*60*24);
						}
					}
				}
				if (gogo){
					delayed_send(PID_CLOCK, command, 1000);
					// print the time
					sendMessage(timer,clockOn);
				}
			} else if(command->mtext[2] == 'T')  {
				clockOn = false;
				// clean the top right corner
				sendMessage(timer,clockOn);
				release_memory_block(command);
			} else {
				uart1_put_string("Wall timer Error: cmd registration invalid\r\n");
				release_memory_block(command);
			}
	  }	else if (sender_id == PID_CLOCK){
			
			// clock is not on
			if(!clockOn){
				release_memory_block(command);
				isRun--;
			}
			// clock is on
			else{
				// remove the extra message loops
				if (isRun>1){
					release_memory_block(command);
					isRun--;
				}else if (isRun) {
					timer++;
					timer %= 24 * 60 * 60;
					delayed_send(PID_CLOCK, command, 1000);
							
					sendMessage(timer,clockOn);
			  }else{
				  // this should never happen
				  uart1_put_string("[Wall ERROR] unreachable code is reached!!");
			  }
		  }
	  }
  }
}
