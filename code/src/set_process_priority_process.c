#include "set_process_priority_process.h"
#include "rtx.h"
#include "uart_polling.h"
#include "k_inc.h"
#include "string.h"
/**************************************************************************//**
 * @brief  User process used to set priority of other processes
 Expect input: %C process_id new_priority
 *****************************************************************************/

void	set_proc_priority_proc(void) {
	MSG_BUF* p_msg;
	int sender_id;
	int pid = 0;
	int new_priority = 0;
	int index;
	void *p_blk;
	MSG_BUF* p_tmp_msg;

	//registration
	p_msg = (MSG_BUF *)request_memory_block();
	p_msg->mtype = KCD_REG;
	p_msg->mtext[0] = '%';
	p_msg->mtext[1] = 'C';
	p_msg->mtext[2] = '\0';
	send_message(PID_KCD, (void *) p_msg);
	while (TRUE) {
		p_msg = receive_message(&sender_id);
		if (sender_id == PID_KCD && p_msg->mtype == KCD_CMD && 
				p_msg->mtext[0] == '%' && p_msg->mtext[1] == 'C') {
			//parse input
					index = 3;
					parse_int(&index, p_msg, &pid);
					parse_int(&index, p_msg, &new_priority);
					if (pid >= PID_NULL && pid <= PID_UART_IPROC &&
							new_priority >= HIGH && new_priority <= PRI_NULL) {
								if (RTX_ERR == set_process_priority(pid, new_priority)) {
									uart1_put_string("[ERROR] Set Priority process set priority failed.\r\n");
								}
					}else {
						//error: illegal parameter
						//log error
						p_blk = request_memory_block();
						p_tmp_msg = p_blk;
						p_tmp_msg->mtype = CRT_DISPLAY;
						str_cpy(p_tmp_msg->mtext, "Invalid command input\r\n");
						if (RTX_ERR == send_message(PID_CRT, p_blk)) {
							uart1_put_string("[ERROR] Set Priority process send message failed.\r\n");
						}
						uart1_put_string("Invalid command input\r\n");
					}
		}
		if (RTX_ERR == release_memory_block(p_msg)) {
				uart1_put_string("[ERROR] Set Priority process releasing memory block failed.\r\n");
		}
	}
}
