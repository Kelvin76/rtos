/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTOS LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *
 *          This software is subject to an open source license and 
 *          may be freely redistributed under the terms of MIT License.
 ****************************************************************************
 */

/**************************************************************************//**
 * @file        ae_proc2.c
 * @brief       test switching between different processes when 
 *              allocating/releasing memory blocks on heap
 *              
 * @version     V1.2021.02
 * @authors     RY
 * @date        2021 Feb
 * @note        Each process is in an infinite loop. Processes never terminate.
 * @details
 *
 *****************************************************************************/

#include "rtx.h"
#include "uart_polling.h"
#include "ae_proc.h"
#include "printf.h"


int ok_counter = 0;
int total_test = 3;
int proc_counter = 1;
/**************************************************************************//**
 * @brief: a process that reserves a memory block, later releases it
 *****************************************************************************/
void proc1(void)
{
    uart1_put_string("G005_test: START\r\n"); 
    printf("G005_test: total %d tests\r\n", total_test); 
    void *p_mem_blk;
    
    p_mem_blk = request_memory_block();
    set_process_priority(PID_P1, LOW);
    release_memory_block(p_mem_blk); 
    set_process_priority(PID_P1, LOWEST);
    printf("G005_test: %d/%d test OK\n\r", ok_counter, total_test);
    printf("G005_test: %d/%d test FAIL\n\r", total_test - ok_counter, total_test);
    printf("G005_test: END\n\r");
    while ( 1 ) {
        release_processor();
    }
   
}
/**************************************************************************//**
 * @brief  a process with LOW priority, blocked by memory, later gets its
           requested memory in the third place
 *****************************************************************************/
void proc2(void)
{
    void *p_mem_blk;
    p_mem_blk = request_memory_block();
#ifdef DEBUG_0
    printf("proc2: p_mem_blk=0x%x\n", p_mem_blk);
#endif /* DEBUG_0 */
    // Within the same priority level, a first-come-first-served policy applies.
    if (proc_counter == 3) {
        uart1_put_string("G005_test: test 3 OK\r\n");
        ok_counter++;
    } else {
        uart1_put_string("G005_test: test 3 FAIL\n\r");
    }
    release_memory_block(p_mem_blk);
    set_process_priority(PID_P2, LOWEST);
    while ( 1 ) {
        release_processor();
    }
}
/**************************************************************************//**
 * @brief  a process with MEDIUM priority, blocked by memory, later gets its
           requested memory in the first place
 *****************************************************************************/
void proc3(void)
{
    void *p_mem_blk;
    int i = 0;
    while (i < 32) {
        p_mem_blk = request_memory_block();
        i++;
    }
#ifdef DEBUG_0
    printf("proc3: p_mem_blk=0x%x\n", p_mem_blk);
#endif /* DEBUG_0 */
    // The highest priority waiting process will get the freed memory block.
    if (proc_counter == 1) {
        uart1_put_string("G005_test: test 1 OK\r\n");
        ok_counter++;
    } else {
        uart1_put_string("G005_test: test 1 FAIL\n\r");
    }
    proc_counter++;
    release_memory_block(p_mem_blk);
    set_process_priority(PID_P3, LOWEST);
    while ( 1 ) {
        release_processor();
    }
}
/**************************************************************************//**
 * @brief  a process with MEDIUM priority, blocked by memory, later gets its
           requested memory in the second place
 *****************************************************************************/
void proc4(void)
{
    void *p_mem_blk;
    p_mem_blk = request_memory_block();
#ifdef DEBUG_0
    printf("proc4: p_mem_blk=0x%x\n", p_mem_blk);
#endif /* DEBUG_0 */
    // The highest priority waiting process will get the freed memory block.
    if (proc_counter == 2) {
        uart1_put_string("G005_test: test 2 OK\r\n");
        ok_counter++;
    } else {
        uart1_put_string("G005_test: test 2 FAIL\n\r");
    }
    proc_counter++;
    release_memory_block(p_mem_blk);
    set_process_priority(PID_P4, LOWEST);
    while ( 1 ) {
        release_processor();
    }
}

void proc5(void)
{
    while(1) {
        //uart1_put_string("proc5: \n\r");
        release_processor();
    }
}

void proc6(void)
{
    while(1) {
        //uart1_put_string("proc6: \n\r");
        release_processor();
    }
}
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
