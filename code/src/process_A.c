#include "process_A.h"
/**************************************************************************//**
 * @brief  The process A
 *****************************************************************************/

void	proc_a(void) {
#ifdef DEBUG_0
	printf("==================================\r\n");
	printf("[DEBUG_0] Process A is entered. \r\n");
#endif
	int     sender_id;
  MSG_BUF *p;

  p = (MSG_BUF *)request_memory_block();
	p->mtype = KCD_REG;
	p->mtext[0] = '%';
	p->mtext[1] = 'Z';
	p->mtext[2] = '\0';
  send_message(PID_KCD, (void *) p);
  while (TRUE) {
    p = receive_message(&sender_id);
    if (p->mtext[0] == '%' && p->mtext[1] == 'Z') {
      release_memory_block((void*) p);
      break;
    } else {
      release_memory_block((void*) p);
    }
  }
  int num = 0;
	while (TRUE) {
     p = (MSG_BUF *)request_memory_block();
     p->mtype = COUNT_REPORT;
     // set message_text field of p to num
	   int digits = 0;
	   int num_copy = num;
	   while (num_copy / 10 != 0) {
       digits++;
			 num_copy = num_copy / 10;
	   }
	   p->mtext[digits + 1] = '\0';
	   num_copy = num;
	   while (num_copy / 10 != 0) {
       p->mtext[digits--] = num_copy % 10 + '0';
	     num_copy = num_copy / 10;
	   }
	   p->mtext[digits] = num_copy + '0';
	   send_message(PID_B, (void *) p);
	   num = num + 1;
     release_processor();
  }
}
