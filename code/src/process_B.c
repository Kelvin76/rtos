#include "process_B.h"
/**************************************************************************//**
 * @brief  The process B
 *****************************************************************************/

void	proc_b(void) {
#ifdef DEBUG_0
	printf("==================================\r\n");
	printf("[DEBUG_0] Process B is entered. \r\n");
#endif
	int sender_id;
  MSG_BUF *p;

  while (TRUE) {
		p = receive_message(&sender_id);
    send_message(PID_C, (void *) p);
  }
}
