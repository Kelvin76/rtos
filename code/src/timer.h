/**
 * @brief timer.h - Timer header file
 * @author Y. Huang
 * @date 2013/02/12
 */
#ifndef _TIMER_H_
#define _TIMER_H_

#include <stdint.h>
#include "k_process.h"
#include "k_msg.h" 
#include "queue.h" 
#include "uart_def.h"

 /*
 *==========================================================================
 *                            GLOBAL VARIABLES
 *==========================================================================
 */

extern queue    g_timeoutQ;						/* timeout queue for the delayed send messages */

 /*
 *===========================================================================
 *                            FUNCTION PROTOTYPES
 *===========================================================================
 */

extern uint32_t timer_init(uint8_t n_timer);	/* initialize timer n_timer */
extern void     timer0_iproc(void);				/* The timer0-i-process used for regular interrupts */
extern int      k_get_current_time(void);		/* Get the current time */
#endif /* ! _TIMER_H_ */

/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
