#ifndef K_STRING_H_
#define K_STRING_H_

#include "k_msg.h"
/*
 *===========================================================================
 *                            FUNCTION PROTOTYPES
 *===========================================================================
 */

// string utilities

extern void str_cpy(char* dest, char* src); /* copy content from src to dest */
extern short ascii_to_idx(char ch); /* convert ascii char to index */
/* parse space delimited int ffrom msg
	 index: start index
	 p_msg: msg to parse from
	 out: output int
*/
extern void parse_int(int* index, MSG_BUF* p_msg, int* out); 
#endif
