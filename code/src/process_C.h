#ifndef K_PROCESS_C_H_
#define K_PROCESS_C_H_

#include "common.h"
#include "rtx.h"
#include "uart_polling.h"
#include "queue.h"
#include "string.h"

// the process C
extern void proc_c(void);

#endif
