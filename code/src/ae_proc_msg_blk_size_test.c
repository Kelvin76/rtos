/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTOS LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *
 *          This software is subject to an open source license and 
 *          may be freely redistributed under the terms of MIT License.
 ****************************************************************************
 */

/**************************************************************************//**
 * @file        ae_proc_mem_priviledge_test.c
 * @brief       test memory api and previledge issues
 *							expect output: test 1 -> 2 -> 3 -> 4
 *              
 * @version     V1.2021.02
 * @authors     JL
 * @date        2021 Feb
 * @note        Each process is in an infinite loop. Processes never terminate.
 * @details
 *
 *****************************************************************************/

#include "rtx.h"
#include "uart_polling.h"
#include "ae_proc.h"
#include "printf.h"
#include "k_memory.h"
#include <LPC17xx.h>

int ok_counter = 0;
int total_test = 2;
/**************************************************************************//**
 * @brief: a process that tests send_message illegal value
 *****************************************************************************/
void proc1(void)
{	
	int retval;
	MSG_BUF* envlope;
	//expected size in byte
	//int expect_env_size = MEM_BLK_SIZE - sizeof(mem_blk) - 36 (msg header size);
	int expect_env_size = MEM_BLK_SIZE - sizeof(mem_blk) - 36;
	uart1_put_string("G005_test: START\r\n"); 
	printf("G005_test: total %d tests\r\n", total_test); 
	envlope = request_memory_block();
	for (retval = 0; retval < expect_env_size; retval++) {
		envlope->mtype = KCD_REG;
		envlope->mtext[retval] = 't';
	}
	retval = send_message(PID_P2, (void*) envlope);
	if (retval == RTX_OK) {
			uart1_put_string("G005_test: test 1 OK\r\n");
			ok_counter++;
	} else {
			uart1_put_string("G005_test: test 1 FAIL\n\r");
	}
	
	set_process_priority(PID_P1, LOW);
	

	while ( 1 ) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief  a process that does nothing.
 *****************************************************************************/
void proc2(void)
{
	void *p_mem_blk;
	uart1_put_string("G005_test: proc 2 starts running OK\r\n");
	for (int i = 0; i < MEM_NUM_BLKS + 10; i++) {
		p_mem_blk = request_memory_block();
		*(int*)p_mem_blk = 0xfeedface;
		printf("G005_test: P2 write to %d th memory block: %x \r\n", i, *(int*)p_mem_blk);
		release_memory_block(p_mem_blk);
	}

	printf("G005_test: P2 OK \r\n");
	ok_counter++;
	printf("G005_test: %d/%d test OK\n\r", ok_counter, total_test);
	printf("G005_test: %d/%d test FAIL\n\r", total_test - ok_counter, total_test);
	printf("G005_test: END\n\r");
	while ( 1 ) {	
		release_processor();
	}
}

/**************************************************************************//**
 * @brief  a process that unblocks p3 but is not preempted.
 *****************************************************************************/

void proc3(void)
{

    while(1) {
        release_processor();
    }
}

void proc4(void)
{
    while(1) {
        uart1_put_string("proc4: \n\r");
        release_processor();
    }
}

void proc5(void)
{
    while(1) {
        uart1_put_string("proc5: \n\r");
        release_processor();
    }
}

void proc6(void)
{
    while(1) {
        uart1_put_string("proc6: \n\r");
        release_processor();
    }
}
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
