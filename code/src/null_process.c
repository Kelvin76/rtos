#include "null_process.h"

/**************************************************************************//**
 * @brief  The Null process (preemptive), which has the lowest priority
 *         and runs only when all other processes are unavailable.
 *****************************************************************************/
void null_proc(void) {
#ifdef DEBUG_NULL_PROC 
	printf("==================================\r\n");
	printf("[DEBUG NULL PROC] NULL Proc () is called. \r\n");
#endif
	while (1) {}
}
