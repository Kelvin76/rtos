/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTX LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *                          All rights reserved.
 *---------------------------------------------------------------------------
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice and the following disclaimer.
 *
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
/**************************************************************************//**
 * @brief   KCD test
 * @author  JL. Yiqing Huang
 * @date    2020/03/27
 * @note    Each process is in an infinite loop. Processes never terminate.
 *****************************************************************************/

/*------------------------------------------------------------------------*
Remember to set preprocessor DEBUG_KCD for this to work
-------------------------------------------------------------------------*/

#include "ae_proc.h"
#include "ae.h"
#include "uart_polling.h"
#include "printf.h"
#include "string.h"

int ok_counter = 0;
int total_test = 0;

/* initialization table item */
void set_test_procs(PROC_INIT *procs, int num)
{
    int i;
    for( i = 0; i < num; i++ ) {
        procs[i].m_pid        = (U32)(i+1);
        procs[i].m_stack_size = 0x300;
    }
  
    procs[0].mpf_start_pc = &proc1;
    procs[0].m_priority   = MEDIUM;
    
    procs[1].mpf_start_pc = &proc2;
    procs[1].m_priority   = MEDIUM;
    
    procs[2].mpf_start_pc = &proc3;
    procs[2].m_priority   = LOW;
    
    procs[3].mpf_start_pc = &proc4;
    procs[3].m_priority   = LOWEST; 
    
    procs[4].mpf_start_pc = &proc5;
    procs[4].m_priority   = LOWEST;
    
    procs[5].mpf_start_pc = &proc6;
    procs[5].m_priority   = LOWEST;
    
}

/**
 * @brief sends crt display requests
 */
void proc1(void)
{
	  MSG_BUF *p_msg;

    uart1_put_string("proc1: entering...\n\r");
    p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = CRT_DISPLAY;
		p_msg->mtext[0] = '%';
		p_msg->mtext[1] = '1';
		p_msg->mtext[2] = '\r';
	  p_msg->mtext[3] = '\n';
	  p_msg->mtext[4] = '\0';
    send_message(PID_CRT, (void *) p_msg);

	  p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = CRT_DISPLAY;
		p_msg->mtext[0] = '%';
		p_msg->mtext[1] = '2';
		p_msg->mtext[2] = '\r';
	  p_msg->mtext[3] = '\n';
		p_msg->mtext[4] = '\0';
    send_message(PID_CRT, (void *) p_msg);

	  p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = PID_KCD;
		p_msg->mtext[0] = 'W';
		p_msg->mtext[1] = 'r';
		p_msg->mtext[2] = 'o';
		p_msg->mtext[3] = 'n';
		p_msg->mtext[4] = 'g';
		p_msg->mtext[5] = ' ';
		p_msg->mtext[6] = 't';
		p_msg->mtext[7] = 'y';
		p_msg->mtext[8] = 'p';
		p_msg->mtext[9] = 'e';
		p_msg->mtext[10] = '\0';
    send_message(PID_CRT, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = CRT_DISPLAY;
		str_cpy(p_msg->mtext, "123456789x123456789x123456789x123456789x123456789x123456789x1234");
    send_message(PID_CRT, (void *) p_msg);
		
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = PID_KCD;
		p_msg->mtext[0] = 'W';
		p_msg->mtext[1] = 'r';
		p_msg->mtext[2] = 'o';
		p_msg->mtext[3] = 'n';
		p_msg->mtext[4] = 'g';
		p_msg->mtext[5] = ' ';
		p_msg->mtext[6] = 't';
		p_msg->mtext[7] = 'y';
		p_msg->mtext[8] = 'p';
		p_msg->mtext[9] = 'e';
		p_msg->mtext[10] = '\0';
    send_message(PID_CRT, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = CRT_DISPLAY;
		str_cpy(p_msg->mtext, "123456789x123456789x123456789x123456789x123456789x123456789x12\n");
    send_message(PID_CRT, (void *) p_msg);

	  p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = CRT_DISPLAY;
		p_msg->mtext[0] = 'a';
		p_msg->mtext[1] = 'b';
		p_msg->mtext[2] = 'c';
		p_msg->mtext[3] = '\r';
	  p_msg->mtext[4] = '\n';
		p_msg->mtext[5] = '\0';
    delayed_send(PID_CRT, (void *) p_msg, 100);

		while(1){}
}

void proc2(void)
{
    uart1_put_string("proc2:\n\r");
    while(1) {
        release_processor();
    }
}

void proc3(void)
{
    uart1_put_string("proc3:\n\r");
    while(1) {
        release_processor();
    }
}

void proc4(void)
{
    uart1_put_string("proc4:\n\r");
    while(1) {
        release_processor();
    }
}

void proc5(void)
{
    while(1) {
        uart1_put_string("proc5:\n\r");
        release_processor();
    }
}

void proc6(void)
{
    while(1) {
        uart1_put_string("proc6:\n\r");
        release_processor();
    }
}
