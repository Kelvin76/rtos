
# RTOS

## Introduction

This is a project of a real time operating system (RTOS). This project is developed based on uVision 5 and Keil MCB1700 Cortex-M3 Board.

## Goal of this project

- Understanding the basics of an operating system by developing this application
- Design, Implement and Test a small RTX
  - Basic multiprogramming environment
  - Five Priority queues and supporting preemption
  - Simple memory management
  - Message-based Interprocess Communication(IPC)
  - Basic timing Services
  - System console I/O (UART 0)
  - Debugging support (UART 1)

---

## Some detailed information

The directory `code/` stores the uVision 5 application.  
The source code is primarily stored in `code/src`.  
Test related files will start with "ae".  
Customized process has name of "\<process name\>\_process.c"  
The stress testing processes are 
"process\_\<A|B|C\>.c"  
We implemented helper functions for queues in "queue.c"
and strings in "string.c"  
I processes are implemented in uart_irq.c and timer.c.

### preprocessors

`DEBUG_0` is used for the main debugging output.  
`DEBUG_MEM` is used for memory related debugging messages.  
`DEBUG_PROC` is used for processes related debugging messages.  
`DEBUG_MSG` is used for message related debugging messages.  
`DEBUG_TIMER` and `DEBUG_TOTALWAIT` is used for timer related debugging messages.  
`DEBUG_CURTIME` is used for get_current_time related debugging messages.  
`DEBUG_UART0_` is for debugging UART_I_Process  
`DEBUG_KCD` is for debugging KCD process. It sends a 
copy of its message to P2, so that we can use P2 to 
test KCD.  
`K_MSG_ENV` is defined.  
`_DEBUG_HOTKEYS` is by default enabled, it enables the standard and bonus debug hotkeys.


