/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTX LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *                          All rights reserved.
 *---------------------------------------------------------------------------
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice and the following disclaimer.
 *
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
 

/**************************************************************************//**
 * @file        k_msg.c
 * @brief       kernel message passing source code template
 *              
 * @version     V1.2021.01
 * @authors     Yiqing Huang
 * @date        2021 JAN
 *****************************************************************************/
#include "k_msg.h"
#include "k_rtx.h"
#include "queue.h"
#include "printf.h"
#include "uart_polling.h"
#include "k_memory.h"

/*
*==========================================================================
*                            Global variables
*==========================================================================
*/

MSG_Record* send_msg_queue;
MSG_Record* receive_msg_queue;

/*
 *==========================================================================
 *                            Functions
 *==========================================================================
 */
int k_send_message(int pid, void *p_msg)
{
		PCB* receiver_pcb = NULL;
		if (p_msg == NULL) {
			//envelope is null
			return RTX_ERR;
		}
		// validate the message
		if (k_validate_mem_blk((U8*)p_msg - sizeof(mem_blk)) != RTX_OK) {
			return RTX_ERR;
		}
		//set fields in envelope
		if (gp_current_process->m_pid != PID_TIMER_IPROC) {
			((MSG_BUF*) p_msg)->m_send_pid = gp_current_process->m_pid;
		}
		((MSG_BUF*) p_msg)->m_recv_pid = pid;
		//get the pcb of receiver
		for (int i = 0; i < NUM_PROCS; i++ ) {
			if ((gp_pcbs[i])->m_pid == pid) {
				receiver_pcb = gp_pcbs[i];
			}
		}
		if (receiver_pcb == NULL) {
			//receiver doesn't exist
			return RTX_ERR;
		}

		// update the message queue for send
		k_msg_queue_push(true, gp_current_process->m_pid, pid, ((MSG_BUF*)p_msg)->mtype, ((MSG_BUF*)p_msg)->mtext);

		//transfer ownership
		if (k_transfer_ownership(p_msg, pid) != RTX_OK) {
			#if defined(DEBUG_0) || defined(DEBUG_MEM) || defined(DEBUG_MSG)
				printf("Transfer message ownership faild : from process %02d to process %02d\n", ((MSG_BUF*) p_msg)->m_send_pid, ((MSG_BUF*) p_msg)->m_recv_pid);
			#endif
			return RTX_ERR;
		}
		//put envelope in the msg queue of receiver
		enqueue(&(receiver_pcb->msg_queue), p_msg);
		#ifdef DEBUG_MSG
		printf("Proc %d has %d messages \r\n", pid, receiver_pcb->msg_queue.size);
		#endif
		//unblock and preemption
		if (receiver_pcb->m_state == BLK_MSG) {
			//expect receiver to be put at the back of ready queue
			k_set_process_state(receiver_pcb->m_pid, receiver_pcb, RDY); 
		}
    return RTX_OK;
}

int k_delayed_send(int pid, void *p_msg, int delay)
{
	// get current time (in case of preemption, reduce the error as much as possible)
	uint32_t currentTime = k_get_current_time();
	
	// check the pid
	if (pid < PID_NULL || pid > PID_UART_IPROC || pid == PID_TIMER_IPROC){
		return RTX_ERR;
	}
	
	//envelope is null or null message
	if (delay < 0 || p_msg == NULL) {
		return RTX_ERR;
	}

	//validate envelope as a memory block
	if (k_validate_mem_blk((U8*)p_msg - sizeof(mem_blk)) != RTX_OK) {
			return RTX_ERR;
	}
	// delay is zero, so send directly
	if (delay == 0) {
		return k_send_message(pid, p_msg);
	}

	// otherwise, we send the message to the mailbox of timer_iproc
	// set fields in envelope
	((MSG_BUF*)p_msg)->m_send_pid = gp_current_process->m_pid;
	((MSG_BUF*)p_msg)->m_recv_pid = pid;
	((MSG_BUF*)p_msg)->m_kdata[1] = delay - (k_get_current_time() - currentTime);	// set the timeout value

	// update the message queue for send
	k_msg_queue_push(true, gp_current_process->m_pid, pid, ((MSG_BUF*)p_msg)->mtype, ((MSG_BUF*)p_msg)->mtext);

	//transfer ownership
	if (k_transfer_ownership(p_msg, PID_TIMER_IPROC) != RTX_OK) {
		#if defined(DEBUG_0) || defined(DEBUG_MEM) || defined(DEBUG_MSG)
			printf("Transfer message ownership faild : from process %02d to process %02d\n", ((MSG_BUF*) p_msg)->m_send_pid, ((MSG_BUF*) p_msg)->m_recv_pid);
		#endif
		return RTX_ERR;
	}
	// add the message into the mailbox of timer0
	enqueue(&gp_pcb_timer_iproc->msg_queue, (node*)p_msg);

	return RTX_OK;
}

void *k_receive_message(int *p_pid)
{
	while (gp_current_process->msg_queue.size == 0) {
		if (gp_current_process->m_state == IPROC) {
			// Non-blocking for i-processes
			return NULL;
		}
		k_set_process_state(gp_current_process->m_pid, gp_current_process, BLK_MSG);
		process_switch();

	}
	MSG_BUF* env = (MSG_BUF*) dequeue(&(gp_current_process->msg_queue));
	if (p_pid != NULL) {
		*p_pid = env->m_send_pid;
	}

	// update the message queue for receive
	k_msg_queue_push(false, env->m_send_pid, env->m_recv_pid, env->mtype, env->mtext);

    return env;
}
 

// helper functions for UART0

void k_msg_queue_push(bool isSend, U8 sender_id, U8 receiver_id, U8 msg_type, char* msg_content) {
	
	// ignore timer messages 
	if(sender_id == PID_TIMER_IPROC){
		return;
	}
	
	// get the timestamp first
	U32 timeStamp = k_get_current_time();
	// get the corresponding queue
	MSG_Record* records = (isSend)? send_msg_queue: receive_msg_queue;

	char* ptr1 = msg_content;
	char* ptr2;
	int ptr_counter = 0;

	// populate the data

	if (records->size >= MSG_QUEUE_SIZE) {
		// the queue is full, need to pop one
		// modify the earliest record

		records->sender[records->first_record] = sender_id;
		records->receiver[records->first_record] = receiver_id;
		records->msg_type[records->first_record] = msg_type;
		records->time_stamp[records->first_record] = timeStamp;
		ptr2 = &records->msg_content[records->first_record] + records->first_record * MSG_CONTENT_PREVIEW;
		while (ptr_counter++ < MSG_CONTENT_PREVIEW) {
			*ptr2++ = *ptr1++;
		}

		records->first_record = (records->first_record + 1) % MSG_QUEUE_SIZE;
	}
	else {
		// add at the end
		records->sender[records->size] = sender_id;
		records->receiver[records->size] = receiver_id;
		records->msg_type[records->size] = msg_type;
		records->time_stamp[records->size] = timeStamp;
		ptr2 = &records->msg_content[records->size] + records->size * MSG_CONTENT_PREVIEW;
		while (ptr_counter++ < MSG_CONTENT_PREVIEW) {
			*ptr2++ = *ptr1++;
		}

		records->size++;
	}

}

void print_msg_content(char* p) {
	char* ptr = p;
	int ptr_counter = 0;
	while (ptr_counter++ < MSG_CONTENT_PREVIEW) {
		uart1_put_char(*ptr++);
	}
}

void k_print_msg_queue(bool isSend) {
	int counter = 0;
	int index;
	MSG_Record* records;

	// print the queue
	if (isSend) {
		records = send_msg_queue;
		printf("\n\r[UART0] %d most recent IPC Send:\n\r", MSG_QUEUE_SIZE);
	}
	else {
		records = receive_msg_queue;
		printf("\n\r[UART0] %d most recent IPC Receive:\n\r", MSG_QUEUE_SIZE);
	}

	index = records->first_record;
	
	// the queue is not yet full
	if (records->size < MSG_QUEUE_SIZE) {
		while (index++ < records->size - 1) {
			printf("%d. Sender:%d. Receiver:%d. Message type:%d. Time stamp:%d. Message preview:",
				++counter, records->sender[index], records->receiver[index], records->msg_type[index],
				records->time_stamp[index]);
			print_msg_content(&records->msg_content[index]);
			uart1_put_string("\n\r");
		}

		// fill the 10 (MSG_QUEUE_SIZE) lines
		while (counter < MSG_QUEUE_SIZE) {
			printf("%d.N/A.\n\r", ++counter);
		}
	}
	// the queue is full
	else {
		while (counter++ < MSG_QUEUE_SIZE) {
			printf("%d. Sender:%d. Receiver:%d. Message type:%d. Time stamp:%d. Message preview:", 
				counter, records->sender[index], records->receiver[index], records->msg_type[index], 
				records->time_stamp[index]);
			print_msg_content(&records->msg_content[index]);
			uart1_put_string("\n\r");
			index = (index + 1) % MSG_QUEUE_SIZE;
		}
	}


}

/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
