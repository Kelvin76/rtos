#include "process_C.h"
/**************************************************************************//**
 * @brief  The process C
 *****************************************************************************/

void	proc_c(void) {
#ifdef DEBUG_0
	printf("==================================\r\n");
	printf("[DEBUG_0] Process C is entered. \r\n");
#endif
	queue message_queue = {.size=0, .head = NULL, .tail = NULL};
	queue* message_queue_ptr = &message_queue;
	int sender_id;
  MSG_BUF *p;
  MSG_BUF *q;

  while (TRUE) {
		if (queue_isEmpty(message_queue_ptr)) {
			p = receive_message(&sender_id);
		} else {
			p = (MSG_BUF *) dequeue(message_queue_ptr);
		}
		if (p->mtype == COUNT_REPORT) {
			int num = 0;
			int i = 0;
			while (p->mtext[i] != '\0') {
				num = num * 10 + (p->mtext[i] - '0');
				i++;
			}
			#ifdef DEBUG_0
			  uart1_put_string(p->mtext);
			  uart1_put_string("\r\n");
			#endif
			if (num % 20 == 0){
				p->mtype = CRT_DISPLAY;
				str_cpy(p->mtext, "Process C\r\n");
				send_message(PID_CRT, (void *) p);
				// hibernate for 10 sec
				q = (MSG_BUF *)request_memory_block();
				q->mtype = WAKEUP10;
				delayed_send(PID_C, q, 10000); // 10 sec delay
				while (true) {
					p = receive_message(&sender_id);
					if (p->mtype == WAKEUP10) {
						break;
					} else {
						enqueue(message_queue_ptr, (node*) p);
					}
				}
			}
		}
		release_memory_block((void*) p);
		release_processor();
	}
}
