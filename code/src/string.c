#include "string.h"


/**************************************************************************//**
 * @brief   str_cpy(char* dest, char* src)
 * @return  void
 * @post    content of src is copied to dest
 *****************************************************************************/

void str_cpy(char* dest, char* src) {
	int idx = 0;
	while (src[idx] != '\0') {
		dest[idx] = src[idx];
		idx++;
	}
	dest[idx] = '\0';
}

/**************************************************************************//**
 * @brief   ascii_to_idx(char ch)
 * @return  index of ch from 0 ('A') to 51 ('z')
 *****************************************************************************/
short ascii_to_idx(char ch) {
	if ((ch - 'A') >= 0 && (ch - 'A') <= 25) {
		return ch - 'A';
	} else if ((ch - 'a') >= 0 && (ch - 'a') <= 25) {
		return ch - 'a' + 26; //comes after the upper cases
	} else {
		return -1;
	}
}

//parse int from p_msg, start from index, return int as "out"
void parse_int(int* index, MSG_BUF* p_msg, int* out) {
	int tmp_int;	
	*out = -1;//make sure that if input is not valid, we return -1
	while (*index < MAX_CMD_SIZE) {
		if (p_msg->mtext[*index] == ' ') {
			(*index)++;
		} else {
			tmp_int = p_msg->mtext[*index] - '0';
			if (tmp_int >= 0 && tmp_int <= 9) {
				if (*out == -1) {
					*out = tmp_int;
				} else {
					*out = (*out)*10 + tmp_int;
				}
				if (p_msg->mtext[*index+1] == ' ' || 
						p_msg->mtext[*index+1] == '\n' ||
						p_msg->mtext[*index+1] == '\r') {
					(*index)++;
					break;
				}
				(*index)++;
			}else {
				*out = -1;
				break;
			}
		}
	}
}
