/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTOS LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *
 *          This software is subject to an open source license and 
 *          may be freely redistributed under the terms of MIT License.
 ****************************************************************************
 */

/**************************************************************************//**
 * @file        ae_proc_mem_priviledge_test.c
 * @brief       test memory api and previledge issues
 *              
 * @version     V1.2021.02
 * @authors     JL
 * @date        2021 Feb
 * @note        Each process is in an infinite loop. Processes never terminate.
 * @details			Expect output: 123456
 *
 *****************************************************************************/

#include "rtx.h"
#include "uart_polling.h"
#include "ae_proc.h"
#include "printf.h"
#include <LPC17xx.h>

int ok_counter = 0;
int total_test = 8;
/**************************************************************************//**
 * @brief: a process that sets p3 to medium priority and unblocks it
 *****************************************************************************/
void proc1(void)
{	
	int retval;
	MSG_BUF* envlope;
	
	uart1_put_string("2");
	set_process_priority(PID_P3, MEDIUM);
	uart1_put_string("3");

	envlope = request_memory_block();
	retval = send_message(3, (void*) envlope);
	uart1_put_string("4");
	while ( 1 ) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief  a process that does nothing.
 *****************************************************************************/
void proc2(void)
{
	uart1_put_string("5");
	while ( 1 ) {	
		release_processor();
	}
}

/**************************************************************************//**
 * @brief  a process that runs first, try to receive memory, then blocked.
 *****************************************************************************/

void proc3(void)
{
		int sender_id;
		int *sender_id_ptr = &sender_id;
    uart1_put_string("1");
		receive_message(sender_id_ptr); //should block this
		uart1_put_string("6\r\n");
    while(1) {
        release_processor();
    }
}

void proc4(void)
{
    while(1) {
        uart1_put_string("proc4: \n\r");
        release_processor();
    }
}

void proc5(void)
{
    while(1) {
        uart1_put_string("proc5: \n\r");
        release_processor();
    }
}

void proc6(void)
{
    while(1) {
        uart1_put_string("proc6: \n\r");
        release_processor();
    }
}
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
