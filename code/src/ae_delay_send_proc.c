/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTOS LAB
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *
 *          This software is subject to an open source license and
 *          may be freely redistributed under the terms of MIT License.
 ****************************************************************************
 */

 /**************************************************************************//**
  * @file        ae_delay_send_proc.c
  * @brief       Used to test the correctness of delayed_send
  *
  * @version     V1.2021.03
  * @authors     KW
  * @date        2021 March
  * @note        Each process is in an infinite loop. Processes never terminate.
  * @details
  *
  *****************************************************************************/
  /*----------------------------------------------------------------------------
   * Several test cases:
    1. Invalid inputs to delayed_send should return RTX_ERR
        Including: invalid pid = -1, 
        Including: invalid pid = 16, 
        Including: null pointer for msg, 
        Including: negative value for delay
    2. delayed_send with proper value for delay
        Including: instant send (delay = 0)
        Including: 100 ms send (delay = 100)
        Including: 200 ms send (delay = 200)
	3. Other
		Delayed_send to oneself
	4. release_mem before the message is sent by delayed_send
		Including: 100 ms send to process itself (delay = 150) and tries to release the memory right after
		Including: 100 ms send to another process (delay = 100) and tries to release the memory right after
	5. delayed_send the same message sent by delayed_send
		Including: 100 ms send to process itself (delay = 150) and tries to send it again
		Including: 100 ms send to another process (delay = 100) and tries to send it again
	6. test 27 - 37: sequence of 5 delayed_send message with similar delayed value
	7. Test if the timer0 preempted processes with the same priorities.

   * Expected COM1 Output (similar to this):

   * G005_test: START
   * G005_test: total 10 tests
   * G005_test: test 1 OK
   * G005_test: test 2 OK
   * G005_test: test 3 OK
   * G005_test: test 4 OK
   * G005_test: test 5 OK.
   * G005_test: test 6 OK.
   * G005_test: test 7 OK.
   * G005_test: test 8 OK.
   * G005_test: test 9 OK.
   * G005_test: test 10 OK.
   * G005_test: 10/10 tests OK
   * G005_test: 0/10 tests FAIL
   * G005_test: END
	 * G005_test: proc1 finished successfully
	 * G005_test: proc2 finished successfully
	 * G005_test: proc3 finished successfully
	 * G005_test: proc4 finished successfully
	 * G005_test: proc5 finished successfully
	 * G005_test: proc6 finished successfully
	 
	 Note: the sequence of test cases matter.
	 The sequence of the last part does not matter. 
	 It's fine as long as everything is printed out in one piece.
   *---------------------------------------------------------------------------*/
#include "rtx.h"
#include "uart_polling.h"
#include "ae_proc.h"
#include "printf.h"


#define _AE_TIMER_TOTAL_TESTS_ 38
int passed_tests = 0;

/**************************************************************************//**
 * @brief:    a process that was originally with MEDIUM priority
 * @Purpose:  Most messages are sent from this process
 *		 
 *****************************************************************************/
void proc1(void)
{
#ifdef DEBUG_0
	printf("[Process 1] Start executing process 1 \n");
	printf("[Process 1] process 1 priority = %02d \n", get_process_priority(PID_P1));
#endif /* DEBUG_0 */

	int retval = 100;
	MSG_BUF* envlope;
	MSG_BUF* envlope2;
	MSG_BUF* envlope3;
	MSG_BUF* envlope4;
	MSG_BUF* envlope5;
	int time1;
	int time2;
	int sender_id;
	int *sender_id_ptr = &sender_id;

	// Start the test
	printf("G005_test: START \n\r");
	printf("G005_test: total %d tests \n\r", _AE_TIMER_TOTAL_TESTS_);


	envlope = request_memory_block();
	//test 1: invalid target pid - 1
	retval = delayed_send(PID_NULL-1, (void*)envlope, 1000);
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 1 OK\r\n");	// invalid pid
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 1 FAIL\n\r");
	}

	//test 2: invalid target pid - 2
	retval = delayed_send(PID_UART_IPROC+1, (void*)envlope, 1000);
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 2 OK\r\n");	// invalid pid
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 2 FAIL\n\r");
	}

	//test 3: invalid pointer for the envelope
	retval = delayed_send(PID_P2, NULL, 1000);
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 3 OK\r\n");	// invalid pid
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 3 FAIL\n\r");
	}

	//test 4: invalid value for delay
	retval = delayed_send(PID_P2, (void*)envlope, -1);
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 4 OK\r\n");	// invalid pid
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 4 FAIL\n\r");
	}

	//test 6: instant send  (with 0 delay)
	retval = delayed_send(PID_P2, (void*)envlope, 0);
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 6 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 6 FAIL\n\r");
	}

	//test 7: send in 100 ms
	envlope = request_memory_block();
	retval = delayed_send(PID_P2, (void*)envlope, 100);	// PID_2 should receive the message in 100 ms from now
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 7 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 7 FAIL\n\r");
	}

	//test 8: send to one's self in 200 ms
	envlope = request_memory_block();
	retval = delayed_send(PID_P1, (void*)envlope, 200);

	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 8 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 8 FAIL\n\r");
	}
	
	//test 11: reciever of 200 ms delayed send related to test 8
	time1 = get_current_time();
	void* env = receive_message(sender_id_ptr);
	time2 = get_current_time();
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 11 OK\r\n");
		passed_tests++;
	}
	else if(env == NULL) {
		uart1_put_string("G005_test: test 11 FAIL env == NULL\n\r");
	}
	else{
		uart1_put_string("G005_test: test 11 FAIL sender_id_ptr != 1\n\r");
	}	
	//test 12: test if the time interval is around 200ms
	if (time2 - time1 > 195 && time2 - time1 < 205) {
		uart1_put_string("G005_test: test 12 OK\r\n");
		passed_tests++;
	}
	else {
		printf("G005_test: test 12 FAIL, peirod: %d, should be 200.\n\r",time2 - time1);
	}


	// Test if one envelope can be delayed_send more than once (Expected to fail)
	// Test 13-15
	envlope = request_memory_block();
	retval = delayed_send(PID_P2, (void*)envlope, 100);	// First set the delay to 100.
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 13 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 13 FAIL\n\r");
	}
	retval = delayed_send(PID_P2, (void*)envlope, 50);	// then 50
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 14 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 14 FAIL\n\r");
	}	
	retval = delayed_send(PID_P2, (void*)envlope, 0);	// then 0
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 15 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 15 FAIL\n\r");
	}


	// Test if one envelope can be delayed_send more than once to oneself (Expected to fail)
	// Test 16-18
	envlope = request_memory_block();
	retval = delayed_send(PID_P1, (void*)envlope, 150);	// First set the delay to 100.
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 16 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 16 FAIL\n\r");
	}
	retval = delayed_send(PID_P1, (void*)envlope, 50);	// then 50
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 17 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 17 FAIL\n\r");
	}
	retval = delayed_send(PID_P1, (void*)envlope, 0);	// then 0
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 18 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 18 FAIL\n\r");
	}

	// Test if one envelope can be released by the sender before it reached destination (Expected to fail)
	// Test 19-20
	envlope = request_memory_block();
	retval = delayed_send(PID_P2, (void*)envlope, 100);	// Send it to PID_P2
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 19 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 19 FAIL\n\r");
	}
	retval = release_memory_block(envlope);
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 20 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 20 FAIL\n\r");
	}


	// Test if one envelope can be released by the sender before it reached destination (Expected to fail)
	// Test 21-22
	envlope = request_memory_block();
	retval = delayed_send(PID_P1, (void*)envlope, 150);	// Send it to itself
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 21 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 21 FAIL\n\r");
	}
	retval = release_memory_block(envlope);
	if (retval == RTX_ERR) {
		uart1_put_string("G005_test: test 22 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 22 FAIL\n\r");
	}

	// test 25: related to test 16
	env = receive_message(sender_id_ptr);
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 25 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 25 FAIL\n\r");
	}
	if (release_memory_block(env) != RTX_OK) {
		uart1_put_string("G005_test: test 25 releasing memory FAIL\n\r");
	}

	// test 26: related to test 21
	env = receive_message(sender_id_ptr);
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 26 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 26 FAIL\n\r");
	}
	if (release_memory_block(env) != RTX_OK) {
		uart1_put_string("G005_test: test 26 releasing memory FAIL\n\r");
	}


	// test 27-31: send 5 messages within proper time separatrion
	envlope = request_memory_block();
	
	// add the send time as the message
	time2 = get_current_time();
	((MSG_BUF*)envlope)->mtext[0] = time2 % 256;
	((MSG_BUF*)envlope)->mtext[1] = (time2/256) % 256;
	
	envlope2 = request_memory_block();
	envlope3 = request_memory_block();
	envlope4 = request_memory_block();
	envlope5 = request_memory_block();
	retval = delayed_send(PID_P2, (void*)envlope, 100);	// Send it to PID_P2
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 27 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 27 FAIL\n\r");
	}
	retval = delayed_send(PID_P2, (void*)envlope2, 99);	// Send it to PID_P2
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 28 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 28 FAIL\n\r");
	}
	retval = delayed_send(PID_P2, (void*)envlope3, 98);	// Send it to PID_P2
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 29 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 29 FAIL\n\r");
	}
	retval = delayed_send(PID_P2, (void*)envlope4, 97);	// Send it to PID_P2
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 30 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 30 FAIL\n\r");
	}
	retval = delayed_send(PID_P2, (void*)envlope5, 96);	// Send it to PID_P2
	if (retval == RTX_OK) {
		uart1_put_string("G005_test: test 31 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 31 FAIL\n\r");
	}
	
	set_process_priority(PID_P1,LOW);
	
	set_process_priority(PID_P1,LOWEST);
	uart1_put_string("G005_test: proc1 finished successfully.\n\r");

	while (1) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief:    a process that was originally with HIGH priority, 
 *            but it will get blocked as soon as it reaches the receive_message() statement.
 * @Purpose:  Most messages are received by this process
 *
 *****************************************************************************/

void proc2(void)
{

	int sender_id;
	int time1;
	int time2;
	int* sender_id_ptr = &sender_id;
	*sender_id_ptr = 123;

	//test 5: reciever of instant send from test 5
	void* env = receive_message(sender_id_ptr);
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 5 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 5 FAIL\n\r");
	}
	if (release_memory_block(env) != RTX_OK) {
		uart1_put_string("G005_test: test 5 releasing memory FAIL\n\r");
	}

	//test 9: reciever of 1 send delayed send from test 7
	// get current time
	time1 = get_current_time();
	env = receive_message(sender_id_ptr);
	time2 = get_current_time();
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 9 OK\r\n");
		passed_tests++;
	}
	else if(env == NULL) {
		uart1_put_string("G005_test: test 9 FAIL env == NULL\n\r");
	}
	else{
		uart1_put_string("G005_test: test 9 FAIL sender_id_ptr != 1\n\r");
	}		
	if (release_memory_block(env) != RTX_OK) {
		uart1_put_string("G005_test: test 9 releasing memory FAIL\n\r");
	}

	//test 10: test if the time interval is around 100ms
	if (time2 - time1 > 95 && time2 - time1 < 105) {
		uart1_put_string("G005_test: test 10 OK\r\n");
		passed_tests++;
	}
	else {
		printf("G005_test: test 10 FAIL, peirod: %d, should be 100.\n\r",time2 - time1);
	}

	set_process_priority(PID_P1, HIGH);
	set_process_priority(PID_P2, MEDIUM);

	//uart1_put_string("G005_test: test 11 STARTS\n\r");
	
	// test 23: related to test 13
	env = receive_message(sender_id_ptr);
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 23 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 23 FAIL\n\r");
	}
	if (release_memory_block(env) != RTX_OK) {
		uart1_put_string("G005_test: test 23 releasing memory FAIL\n\r");
	}

	// test 24: related to test 19
	env = receive_message(sender_id_ptr);
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 24 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 24 FAIL\n\r");
	}
	if (release_memory_block(env) != RTX_OK) {
		uart1_put_string("G005_test: test 24 releasing memory FAIL\n\r");
	}


	// A sequence of 5 messages

	// test 32, receiving from test 27
	env = receive_message(sender_id_ptr);
	time1 = ((MSG_BUF*)env)->mtext[0] + ((MSG_BUF*)env)->mtext[1]*256;
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 32 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 32 FAIL\n\r");
	}
	
	if (release_memory_block(env) != RTX_OK) {
		uart1_put_string("G005_test: test 32 releasing memory FAIL\n\r");
	}
	// test 33, receiving from test 28
	env = receive_message(sender_id_ptr);
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 33 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 33 FAIL\n\r");
	}
	if (release_memory_block(env) != RTX_OK) {
		uart1_put_string("G005_test: test 33 releasing memory FAIL\n\r");
	}
	// test 34, receiving from test 29
	env = receive_message(sender_id_ptr);
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 34 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 34 FAIL\n\r");
	}
	if (release_memory_block(env) != RTX_OK) {
		uart1_put_string("G005_test: test 34 releasing memory FAIL\n\r");
	}
	// test 35, receiving from test 30
	env = receive_message(sender_id_ptr);
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 35 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 35 FAIL\n\r");
	}
	if (release_memory_block(env) != RTX_OK) {
		uart1_put_string("G005_test: test 35 releasing memory FAIL\n\r");
	}
	// test 36, receiving from test 31
	env = receive_message(sender_id_ptr);
	time2 = get_current_time();
	if (env != NULL && *sender_id_ptr == 1) {
		uart1_put_string("G005_test: test 36 OK\r\n");
		passed_tests++;
	}
	else {
		uart1_put_string("G005_test: test 36 FAIL\n\r");
	}
	if (release_memory_block(env) != RTX_OK) {
		uart1_put_string("G005_test: test 36 releasing memory FAIL\n\r");
	}

	// test 37, check the time period
	if (time2 - time1 > 99 && time2 - time1 < 113) {
		uart1_put_string("G005_test: test 37 OK\r\n");
		passed_tests++;
	}
	else {
		printf("G005_test: test 37 FAIL, peirod: %d, should be 103.\n\r", time2 - time1);
		printf("G005_test: Send: %d.\n\r", time1);
		printf("G005_test: Receive: %d.\n\r", time2);
	}

	// get the time when this message was sent
	// add current time as message body
	env = request_memory_block();
	((MSG_BUF*)env)->mtype = KCD_REG;
	time2 = get_current_time();
	((MSG_BUF*)env)->mtext[0] = time2 % 256;
	((MSG_BUF*)env)->mtext[1] = (time2/256) % 256;
	// send the message
	delayed_send(PID_P2, (void*)env, 150);
	
	// test 38, check the time period
	env = receive_message(sender_id_ptr);
	time2 = get_current_time();
	time1 = ((MSG_BUF*)env)->mtext[0] + ((MSG_BUF*)env)->mtext[1]*256;
	if (time2 - time1 > 148 && time2 - time1 < 158) {
		uart1_put_string("G005_test: test 38 OK\r\n");
		passed_tests++;
	}
	else {
		printf("G005_test: test 38 FAIL, peirod: %d, should be 150.\n\r", time2 - time1);
		printf("G005_test: Send: %d.\n\r", time1);
		printf("G005_test: Receive: %d.\n\r", time2);
	}
	
	// print out for tests
	printf("G005_test: %01d/%01d tests OK\n\r", passed_tests, _AE_TIMER_TOTAL_TESTS_);
	printf("G005_test: %01d/%01d tests FAIL\n\r", _AE_TIMER_TOTAL_TESTS_ - passed_tests, _AE_TIMER_TOTAL_TESTS_);
	uart1_put_string("G005_test: END\n\r");
	
	set_process_priority(PID_P3,LOWEST);
	set_process_priority(PID_P2,LOWEST);
	uart1_put_string("G005_test: proc2 finished successfully.\n\r");
	
	while (1) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief:    a process that was originally with LOW priority, 
 *            This process will take charge of the system when P1 and P2 are both blocked by message
 * @Purpose:  Hold the print out for finishing process
 *
 *****************************************************************************/

void proc3(void)
{

	while (1) {
		if(get_process_priority(PID_P3) == LOWEST){
			// meaning we reached the end of the test process
			break;
		}
		release_processor();
	}
	
	uart1_put_string("G005_test: proc3 finished successfully.\n\r");
	while (1) {
		release_processor();
	}
}

void proc4(void)
{
	uart1_put_string("G005_test: proc4 finished successfully.\n\r");
	while (1) {
		release_processor();
	}
}

void proc5(void)
{
	uart1_put_string("G005_test: proc5 finished successfully.\n\r");
	while (1) {
		release_processor();
	}
}

void proc6(void)
{
	uart1_put_string("G005_test: proc6 finished successfully.\n\r");
	while (1) {
		release_processor();
	}
}
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
