/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTX LAB  
 *
 *        Copyright 2020-2021 Yiqing Huang and NXP Semiconductors
 *                          All rights reserved.
 *---------------------------------------------------------------------------
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice and the following disclaimer.
 *
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
/**************************************************************************//**
 * @file        uart_irq.c
 * @brief       UART IRQ handler. It receives input char through RX interrupt
 *              and then writes a string containing the input char through
 *              TX interrupts. 
 *              
 * @version     V1.2021.01
 * @authors     Yiqing Huang and NXP Semiconductors
 * @date        2021 JAN
 *****************************************************************************/
#include <LPC17xx.h>

#include "common.h"
#include "k_process.h"
#include "k_msg.h"
#include "k_inc.h"
#include "k_memory.h"
#include "uart_def.h"
#include "uart_irq.h"
#include "uart_polling.h"
#include "queue.h"

#include "printf.h"


 /*
  *===========================================================================
  *                             Global variables
  *===========================================================================
  */

uint8_t g_buffer[MAX_CMD_SIZE];
uint8_t *gp_buffer = g_buffer;
uint8_t g_buffer_size = 0;
uint8_t g_char_in;
uint8_t g_char_out;
uint8_t g_send_char = 0;

/**************************************************************************//**
 * @brief: initialize the n_uart
 * NOTES: It only supports UART0. It can be easily extended to support UART1 IRQ.
 * The step number in the comments matches the item number in Section 14.1 on pg 298
 * of LPC17xx_UM
 *****************************************************************************/
int uart_irq_init(int n_uart) {

    LPC_UART_TypeDef *pUart;

    if ( n_uart ==0 ) {
        /*
        Steps 1 & 2: system control configuration.
        Under CMSIS, system_LPC17xx.c does these two steps
         
        -----------------------------------------------------
        Step 1: Power control configuration. 
                See table 46 pg63 in LPC17xx_UM
        -----------------------------------------------------
        Enable UART0 power, this is the default setting
        done in system_LPC17xx.c under CMSIS.
        Enclose the code for your refrence
        //LPC_SC->PCONP |= BIT(3);
    
        -----------------------------------------------------
        Step2: Select the clock source. 
               Default PCLK=CCLK/4 , where CCLK = 100MHZ.
               See tables 40 & 42 on pg56-57 in LPC17xx_UM.
        -----------------------------------------------------
        Check the PLL0 configuration to see how XTAL=12.0MHZ 
        gets to CCLK=100MHZin system_LPC17xx.c file.
        PCLK = CCLK/4, default setting after reset.
        Enclose the code for your reference
        //LPC_SC->PCLKSEL0 &= ~(BIT(7)|BIT(6));    
            
        -----------------------------------------------------
        Step 5: Pin Ctrl Block configuration for TXD and RXD
                See Table 79 on pg108 in LPC17xx_UM.
        -----------------------------------------------------
        Note this is done before Steps3-4 for coding purpose.
        */
        
        /* Pin P0.2 used as TXD0 (Com0) */
        LPC_PINCON->PINSEL0 |= (1 << 4);  
        
        /* Pin P0.3 used as RXD0 (Com0) */
        LPC_PINCON->PINSEL0 |= (1 << 6);  

        pUart = (LPC_UART_TypeDef *) LPC_UART0;     
        
    } else if ( n_uart == 1) {
        
        /* see Table 79 on pg108 in LPC17xx_UM */ 
        /* Pin P2.0 used as TXD1 (Com1) */
        LPC_PINCON->PINSEL4 |= (2 << 0);

        /* Pin P2.1 used as RXD1 (Com1) */
        LPC_PINCON->PINSEL4 |= (2 << 2);          

        pUart = (LPC_UART_TypeDef *) LPC_UART1;
        
    } else {
        return 1; /* not supported yet */
    } 
    
    /*
    -----------------------------------------------------
    Step 3: Transmission Configuration.
            See section 14.4.12.1 pg313-315 in LPC17xx_UM 
            for baud rate calculation.
    -----------------------------------------------------
    */
    
    /* Step 3a: DLAB=1, 8N1 */
    pUart->LCR = UART_8N1; /* see uart.h file */ 

    /* Step 3b: 115200 baud rate @ 25.0 MHZ PCLK */
    pUart->DLM = 0; /* see table 274, pg302 in LPC17xx_UM */
    pUart->DLL = 9;    /* see table 273, pg302 in LPC17xx_UM */
    
    /* FR = 1.507 ~ 1/2, DivAddVal = 1, MulVal = 2
       FR = 1.507 = 25MHZ/(16*9*115200)
       see table 285 on pg312 in LPC_17xxUM
    */
    pUart->FDR = 0x21;       
    
 

    /*
    ----------------------------------------------------- 
    Step 4: FIFO setup.
           see table 278 on pg305 in LPC17xx_UM
    -----------------------------------------------------
        enable Rx and Tx FIFOs, clear Rx and Tx FIFOs
    Trigger level 0 (1 char per interrupt)
    */
    
    pUart->FCR = 0x07;

    /* Step 5 was done between step 2 and step 4 a few lines above */

    /*
    ----------------------------------------------------- 
    Step 6 Interrupt setting and enabling
    -----------------------------------------------------
    */
    /* Step 6a: 
       Enable interrupt bit(s) wihtin the specific peripheral register.
           Interrupt Sources Setting: RBR, THRE or RX Line Stats
       See Table 50 on pg73 in LPC17xx_UM for all possible UART0 interrupt sources
       See Table 275 on pg 302 in LPC17xx_UM for IER setting 
    */
    /* disable the Divisior Latch Access Bit DLAB=0 */
    pUart->LCR &= ~(BIT(7)); 
    
    /* enable RBR and RLS interrupts */
    pUart->IER = IER_RBR | IER_RLS; 
    
    /* Step 6b: enable the UART interrupt from the system level */
    
    if ( n_uart == 0 ) {
        NVIC_EnableIRQ(UART0_IRQn); /* CMSIS function */
    } else if ( n_uart == 1 ) {
        NVIC_EnableIRQ(UART1_IRQn); /* CMSIS function */
    } else {
        return 1; /* not supported yet */
    }
    pUart->THR = '\0';
    return 0;
}


/**
 * @brief: use CMSIS ISR for UART0 IRQ Handler
 * NOTE: This example shows how to save/restore all registers rather than just
 *       those backed up by the exception stack frame. We add extra
 *       push and pop instructions in the assembly routine. 
 *       The actual c_UART0_iproc does the rest of irq handling
 */
__asm void UART0_IRQHandler(void)
{
        PRESERVE8
        IMPORT c_UART0_iproc
        IMPORT  k_run_new_process
        CPSID I
        PUSH{r4-r11, lr}
IUART_SAVE
        // save the sp of the current running process into its PCB
        LDR     R1, =__cpp(&gp_current_process) // load R1 with &gp_current_process
        LDR     R2, [R1]                        // load R2 with gp_current_process value
        STR     SP, [R2, #PCB_MSP_OFFSET]       // save MSP to gp_current_process->mp_sp

        // save the interrupted process's PCB in gp_pcb_interrupted
        LDR     R3, =__cpp(&gp_pcb_interrupted) // load &gp_pcb_interrupted to R3
        STR     R2, [R3]                        // assign gp_current_process to gp_pcb_interrupted

        // update gp_current_process to point to the gp_pcb_UART_iproc
        LDR     R1, =__cpp(&gp_pcb_UART_iproc)  // load R1 with &gp_pcb_UART_iproc
        LDR     R2, [R1]                        // load R2 with gp_pcb_UART_iproc value
        LDR     R3, =__cpp(&gp_current_process) // load R3 with &gp_current_process
        STR     R2, [R3]                        // assign gp_pcb_UART_iproc to gp_current_process

IUART_EXEC
        // update gp_current_process to the PCB of timer_i_proc 
        LDR     R1, =__cpp(&gp_pcb_UART_iproc) // load R1 with &gp_pcb_UART_iproc 
        LDR     R2, [R1]                        // load R2 with gp_pcb_UART_iproc value
        LDR     SP, [R2, #PCB_MSP_OFFSET]       // load MSP with UART0_IPROC's SP (i.e. gp_pcb_UART_iproc->mp_sp)
        BL      c_UART0_iproc                   // execute the UART i-process

IUART_RESTORE
        // updae the gp_current_process to gp_pcb_interrupted
        LDR     R1, =__cpp(&gp_pcb_interrupted) // load R1 with &gp_pcb_interrupted
        LDR     R2, [R1]                        // load R2 with gp_pcb_interrupted value
        LDR     R3, =__cpp(&gp_current_process) // load R3 with &gp_current_process
        STR     R2, [R3]                        // assign gp_pcb_interrupted to gp_current_process

        // restore the interrupted process's PCB to gp_current_process
        LDR     R1, =__cpp(&gp_pcb_interrupted)
        LDR     R2, [R1]
        LDR     SP, [R2, #PCB_MSP_OFFSET]       // load MSP with gp_current_process->mp_sp
        BL      k_run_new_process               // run a Non-IPROC 

        CPSIE I
        POP{r4-r11, pc}
} 
/**
 * @brief: c UART0 I Process
 */
void c_UART0_iproc(void)
{
    MSG_BUF* c_msg;
    char* c_ptr;
    int sender_id;
    bool flag = false;

    uint8_t IIR_IntId;        /* Interrupt ID from IIR */          
    LPC_UART_TypeDef *pUart = (LPC_UART_TypeDef *)LPC_UART0;
    
    #ifdef DEBUG_0
        uart1_put_string("Entering c_UART0_iproc\n\r");
    #endif // DEBUG_0

    /* Reading IIR automatically acknowledges the interrupt */
    IIR_IntId = (pUart->IIR) >> 1 ; /* skip pending bit in IIR */ 
    if (IIR_IntId & IIR_RDA) { /* Receive Data Avaialbe */
        /* Read UART. Reading RBR will clear the interrupt */

        // get the input char
        g_char_in = pUart->RBR;

        // TODO:
        // check if this is a hot key
        
        switch(g_char_in){
					// wrap the hot keys under this preprocessor
#ifdef _DEBUG_HOTKEYS
        case '!':
		    // ready processes and their priorities	

            for (int index = 0; index < NUM_PROCS; ++index) {
                if (gp_pcbs[index]->m_state == RDY) {
                    if (!flag) {
                        uart1_put_string("\n\r[UART0] Processes are Ready:\n\r");
                        flag = true;
                    }
                    printf("PID: %d. Priority: %d.\n\r", gp_pcbs[index]->m_pid, gp_pcbs[index]->m_priority);
                }
            }
            if (!flag) {
                uart1_put_string("\n\r[UART0] No ready process exists.\n\r");
            }
            else {
                uart1_put_string("[UART0] End.\n\r");
            }

            break;
        case '@':
            // blocked on memory processes and their priorities

            // check if exists at least one blocked on memory process
            for (int priority = HIGH; priority < NUM_PRIORITIES;) {
                if (queue_isEmpty(&g_blked_mem_queue[priority])) {
                    priority++;
				    if(priority >= NUM_PRIORITIES){
				        flag = true;
				    }
                }else{
				    break;
				}
            }
            // if no process is ready
            if (flag) {
                uart1_put_string("\n\r[UART0] Processes blocked on [Memory] is 0 (zero).\n\r");
            }
            else {
                uart1_put_string("\n\r[UART0] Processes are Blocked on [Memory]:\n\r");
                // Ready processes and their priorities
                for (int index = 0; index < NUM_PROCS; ++index) {
                    if (gp_pcbs[index]->m_state == BLK_MEM) {
                        printf("PID: %d. Priority: %d.\n\r", gp_pcbs[index]->m_pid, gp_pcbs[index]->m_priority);
                    }
                }
			    uart1_put_string("[UART0] End.\n\r");
            }

            break;
        case '#':
            // blocked on receive processes and their priorities
            for (int index = 0; index < NUM_PROCS; ++index) {
                if (gp_pcbs[index]->m_state == BLK_MSG) {
                    if (!flag) {
                        uart1_put_string("\n\r[UART0] Processes are blocked on [Receive]:\n\r");
                        flag = true;
                    }
                    printf("PID: %d. Priority: %d.\n\r", gp_pcbs[index]->m_pid, gp_pcbs[index]->m_priority);
                }
            }
            if (!flag) {
                uart1_put_string("\n\r[UART0] Processes blocked on [Receive] is 0 (zero).\n\r");
            }else{
			    uart1_put_string("[UART0] End.\n\r");
			}

            break;
        case '$':
            // all processesí» priorities and states
            uart1_put_string("\n\r[UART0] All processes priority and states:\n\r");
            for (int index = 0; index < NUM_PROCS; ++index) {
                printf("PID: %d. Priority: %d.", gp_pcbs[index]->m_pid, gp_pcbs[index]->m_priority);
                switch (gp_pcbs[index]->m_state) {
                case NEW:
                    uart1_put_string(" State: NEW.\n\r");
                    break;                
                case RDY:
                    uart1_put_string(" State: RDY.\n\r");
                    break;                
                case RUN:
                    uart1_put_string(" State: RUN.\n\r");
                    break;                
                case BLK_MEM:
                    uart1_put_string(" State: BLK_MEM.\n\r");
                    break;                
                case BLK_MSG:
                    uart1_put_string(" State: BLK_MSG.\n\r");
                    break;                
                case IPROC:
                    uart1_put_string(" State: IPROC.\n\r");
                    break;
                default:
                    /* Should never reach here */
                    uart1_put_string("\n\r");
                    printf("[ERROR] invalid process state:%d, pid:%d\n\r", gp_pcbs[index]->m_state, gp_pcbs[index]->m_pid);
                }
            }
		    uart1_put_string("[UART0] End.\n\r");

            break;
        case '&':
            // number of memory blocks available
            printf("\n\r[UART0] Available memory block: %d.\n\r", mem_pool.size);
		    uart1_put_string("[UART0] End.\n\r");
            break;
        case '^':
            // 10 most recent IPC messages
            k_print_msg_queue(true);
            k_print_msg_queue(false);
		    uart1_put_string("[UART0] End.\n\r");
            break;

#endif /* _DEBUG_HOTKEYS */
        
        /* forward the character to KCD */
        default:
						
				// TODO: remove this
						//uart1_put_string("The user typed "); 
						//uart1_put_char(g_char_in);
						//uart1_put_string(".\n\r");
				
            // initialize the message information
            c_msg = k_request_memory_block_nb();
            if (c_msg != NULL) {
                c_msg->mtype = KEY_IN;
                c_ptr = c_msg->mtext;
                *c_ptr++ = g_char_in;
                *c_ptr = '\0';

                if (RTX_ERR == k_send_message(PID_KCD, c_msg)) {
                    // this should never be executed
                    uart1_put_string("[ERROR] uart_i_proc sending message to KCD FAILED.\n\r");
                    if (RTX_ERR == k_release_memory_block(c_msg)) {
                        // this should never (double never) be executed
                        uart1_put_string("[ERROR] UART_IPROC failed to release the memory block of a message.\n\r");
                    }
                }
            }
            // ignore the key in if no available memory block exists

        }

        #ifdef DEBUG_0
            uart1_put_string("Reading a char = ");
            uart1_put_char(g_char_in);
            uart1_put_string("\n\r");
        #endif /* DEBUG_0 */  

    } else if (IIR_IntId & IIR_THRE) {
        /* THRE Interrupt, transmit holding register becomes empty */

        // if buffer is not empty, print the buffer
        if (g_buffer_size > 0) {
            // it's not the end of the string, keep printing
            if (*gp_buffer != '\0') {
                g_char_out = *gp_buffer;

                #ifdef DEBUG_0
                    /*uart1_put_string("Writing a char = ");
                        uart1_put_char(g_char_out);
                        uart1_put_string("\n\r");
                    */
                    /* you could use the printf instead */
                    printf("Writing a char = %c \n\r", g_char_out);
                #endif /* DEBUG_0 */       

                pUart->THR = g_char_out;
                gp_buffer++;
                g_buffer_size--;
            }        
            // it's the end of the string, print the end character and shut down THRE
            else {

                #ifdef DEBUG_0
                    uart1_put_string("Finish writing. Turning off IER_THRE\n\r");
                #endif /* DEBUG_0 */
                
                // reset the position of gp_buffer to the beginning
                gp_buffer = g_buffer;
                g_buffer_size = 0;
			    // has more message in the mailbox
			    if(--g_send_char == 0){
				    // toggle the IER_THRE bit 
				    pUart->IER ^= IER_THRE;
			    }
			    pUart->THR = '\0';
            }
        }
        else {
            // read a message from the message box
            if (gp_current_process->msg_queue.size > 0) {

                c_msg = k_receive_message(&sender_id);

                // check validity of the message
                // ignore messages sent from processes other than CRT
                if (sender_id != PID_CRT) {
                    if (RTX_ERR == k_release_memory_block(c_msg)) {
                        // this should never be executed
                        uart1_put_string("[ERROR] UART_IPROC failed to release the memory block of a message.\n\r");
                    }
                }else{

				    // put the message content into the buffer
				    g_buffer_size = 0;
				    gp_buffer = g_buffer;
				    c_ptr = c_msg->mtext;
				    while (*c_ptr != '\0') {
							
					    #ifdef DEBUG_UART0_
				        // check if the buffer is the same as the input
					    uart1_put_char(*c_ptr);
				        #endif /* DEBUG_UART0_ */
							
					    g_buffer_size++;
					    *gp_buffer++ = *c_ptr++;
				    }
				    *gp_buffer = *c_ptr;
				    gp_buffer = g_buffer;

				    // input the first element
				    pUart->THR = *gp_buffer;
				    gp_buffer++;
						
			        #ifdef DEBUG_UART0_
				        // check if the buffer is the same as the input
				        gp_buffer--;
				        while (*gp_buffer != '\0') {
				            uart1_put_char(*gp_buffer++);
				        }
				        uart1_put_char(*gp_buffer);
				        gp_buffer = g_buffer;
				        gp_buffer++;
			        #endif /* DEBUG_UART0_ */

				    // deallocate the message
				    if (RTX_ERR == k_release_memory_block(c_msg)) {
					    uart1_put_string("[ERROR] UART_IPROC failed to release the memory block of a message.\n\r");
				    }
						
				}
            } 
        }
    } else {  /* not implemented yet */

         #ifdef DEBUG_UART0_
         uart1_put_string("[ERROR] UART reached unreachable area! IIR_IntId:");
         printf("%d\n\r", IIR_IntId);
         #else

         uart1_put_string("[ERROR] UART reached unreachable area!\n\r");
         #endif /* DEBUG_UART0_ */
         return;
    }    
}

/*
This is for CRT knowledge:
code for enabling THRE:

pUart->IER = IER_THRE | IER_RLS | IER_RBR;

*/

/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
