#ifndef K_PROCESS_A_H_
#define K_PROCESS_A_H_

#include "common.h"
#include "rtx.h"
#include "uart_polling.h"

// the process A
extern void proc_a(void);

#endif
