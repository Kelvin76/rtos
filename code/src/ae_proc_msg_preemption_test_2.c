/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTOS LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *
 *          This software is subject to an open source license and 
 *          may be freely redistributed under the terms of MIT License.
 ****************************************************************************
 */

/**************************************************************************//**
 * @file        ae_proc_mem_priviledge_test.c
 * @brief       test memory api and previledge issues
 *							expect output: test 1 -> 2 -> 3 -> 4
 *              
 * @version     V1.2021.02
 * @authors     JL
 * @date        2021 Feb
 * @note        Each process is in an infinite loop. Processes never terminate.
 * @details			Expected output: 1234
 *
 *****************************************************************************/

#include "rtx.h"
#include "uart_polling.h"
#include "ae_proc.h"
#include "printf.h"
#include <LPC17xx.h>

int ok_counter = 0;
int total_test = 8;
/**************************************************************************//**
 * @brief: a process that was first ran, blocked, then unblocked
 *****************************************************************************/
void proc1(void)
{	
	int sender_id;
	int *sender_id_ptr = &sender_id;
	uart1_put_string("1");
	receive_message(sender_id_ptr); //should block this
	uart1_put_string("4\r\n");	

	while ( 1 ) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief  a process that does nothing.
 *****************************************************************************/
void proc2(void)
{
	while ( 1 ) {	
		release_processor();
	}
}

/**************************************************************************//**
 * @brief  a process that unblocks p3 but is not preempted.
 *****************************************************************************/

void proc3(void)
{
		int retval;
		MSG_BUF* envlope;

		uart1_put_string("2");
		envlope = request_memory_block();
		retval = send_message(1, (void*) envlope);
		uart1_put_string("3");
    while(1) {
        release_processor();
    }
}

void proc4(void)
{
    while(1) {
        uart1_put_string("proc4: \n\r");
        release_processor();
    }
}

void proc5(void)
{
    while(1) {
        uart1_put_string("proc5: \n\r");
        release_processor();
    }
}

void proc6(void)
{
    while(1) {
        uart1_put_string("proc6: \n\r");
        release_processor();
    }
}
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
