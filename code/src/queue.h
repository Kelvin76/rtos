#ifndef K_QUEUE_H_
#define K_QUEUE_H_

#include "common.h"


/* Define the bool variable */
typedef enum{
		false = 0,
		true = ! false
} bool ;


/* This is a general node for queue data structure */
typedef struct node {
    // unsigned int mem_blk* next; todo: why this?
	struct node *next;
	U32 value1; 
	U32 value2; 
	U32 value3; 
	U32* ptr;
	void* mem_addr;
	BOOL control_bit;
} node;

typedef struct queue {
    node* head;
    node* tail;
    int size;
} queue;

/*
 *===========================================================================
 *                            FUNCTION PROTOTYPES
 *===========================================================================
 */

// basic functions used in a queue

extern int enqueue(queue* q, node* n);				/* standard enqueue for a FIFO queue, i.e., enqueue at the end */
extern int enqueue_opposite(queue* q, node* n);		/* opposite enqueue for a FIFO queue, i.e., enqueue at the front */
extern node* dequeue(queue* q);						/* standard dequeue for a FIFO queue, i.e., dequeue from the front */
extern bool is_equal(node* n1, node* n2);			/* test if two nodes are equal (literal instead of addresses) */
extern int queue_remove(queue* q, node *nodeTarget) ;	/* remove the node from the queue */
extern bool queue_isEmpty(queue* q);				/* check if the queue is empty */
extern int queue_getSize(queue* q);					/* get the size of the queue */
#endif
