/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTX LAB  
 *
 *           Copyright 2020-2021 Yiqing Huang and Thomas Reidemeister
 *                          All rights reserved.
 *---------------------------------------------------------------------------
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice and the following disclaimer.
 *
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
 

/**************************************************************************//**
 * @file        k_process.c 
 * @brief       kernel process management C file 
 *              
 * @version     V1.2021.01
 * @authors     Yiqing Huang, Thomas Reidemeister
 * @date        2021 JAN
 * @note        The example code shows one way of implementing context switching.
 *              The code only has minimal sanity check. 
 *              There is no stack overflow check.
 *              The implementation assumes only two simple user processes and 
 *              NO HARDWARE INTERRUPTS. 
 *              The purpose is to show one way of doing context switch
 *              under stated assumptions. 
 *              These assumptions are not true in the required RTX Project!!!
 *              If you decide to use this piece of code, 
 *              you need to understand the assumptions and the limitations.
 *
 *****************************************************************************/

#include "k_process.h"
#include "k_rtx.h"
#include "queue.h" 
#include "uart_def.h"
/*
*==========================================================================
*                            GLOBAL VARIABLES
*==========================================================================
*/
PCB**gp_pcbs;
bool g_is_released = true;		/* Indicates whether the processor is volunteerly released */

PCB* gp_pcb_timer_iproc = NULL; /* points to Timer iprocess pcb */
PCB* gp_pcb_UART_iproc = NULL;  /* points to UART iprocess pcb */
PCB* gp_pcb_interrupted;        /* interrupted process's pcb    */

PCB* gp_current_process = NULL; /* always point to the current RUN process */

/* process initialization table (only for initialization) */
PROC_INIT g_proc_table[NUM_PROCS];  /* Test Procs + Null Proc */

/* scheduler (Priority queue) initialization (only new and rdy procs) */
queue g_scheduler[NUM_PRIORITIES];
/* a queue for the PCBs that are blocked on memory */
queue g_blked_mem_queue[NUM_PRIORITIES];

/**************************************************************************//**
 * @biref initialize all processes in the system
 * @note  We assume there are only two user processes in the system in 
 *        this example starter code.
 *        The preprocessor ECE350_DEMO should be defined
 *****************************************************************************/
void process_init(PROC_INIT *proc_info, int num) 
{
	int i;
	U32 *sp;
  
  /* fill out the initialization table */
	/* this is for the 6 test processes */
	for ( i = 1; i < num+1; i++ ) {
		g_proc_table[i].m_pid        = proc_info[i-1].m_pid;
		g_proc_table[i].m_stack_size = proc_info[i-1].m_stack_size;
		g_proc_table[i].mpf_start_pc = proc_info[i-1].mpf_start_pc;

		// if the priority is out of boundary (or not defined), then we set it to default
		if ((proc_info[i - 1].m_priority) < HIGH && (proc_info[i - 1].m_priority) > PRI_NULL) {
			g_proc_table[i].m_priority = DEFAULT_PRIORITY;
		}
		else {
			g_proc_table[i].m_priority = proc_info[i - 1].m_priority;
		}

	}
  
	/* TODO Change the index into macro constants when all processes are created */

	/* Add null process to the table as the first element */
	g_proc_table[PID_NULL].m_pid		= (U32)(PID_NULL);
	g_proc_table[PID_NULL].m_stack_size	= USR_SZ_STACK;
	g_proc_table[PID_NULL].mpf_start_pc	= &null_proc;
	g_proc_table[PID_NULL].m_priority	= PRI_NULL;
	
	/* Add KCD process to the table as the 8th element */
	g_proc_table[7].m_pid			= (U32)(PID_KCD);  
	g_proc_table[7].m_stack_size	= USR_SZ_STACK;
	g_proc_table[7].mpf_start_pc	= &kcd_proc;
	g_proc_table[7].m_priority		= HIGH;


	/* Add CRT process to the table as the 9th element */
	g_proc_table[8].m_pid			= (U32)(PID_CRT);
	g_proc_table[8].m_stack_size	= USR_SZ_STACK;
	g_proc_table[8].mpf_start_pc	= &crt_proc;
	g_proc_table[8].m_priority		= HIGH;
	

	/* Add SET_PRIORITY process to the table as the 10th element */
	g_proc_table[9].m_pid			= (U32)(PID_SET_PRIO);
	g_proc_table[9].m_stack_size	= USR_SZ_STACK;
	g_proc_table[9].mpf_start_pc	= &set_proc_priority_proc;
	g_proc_table[9].m_priority		= HIGH;

	/* Add process A to the table as the 11th element */
	g_proc_table[10].m_pid			= (U32)(PID_A);
	g_proc_table[10].m_stack_size	= USR_SZ_STACK;
	g_proc_table[10].mpf_start_pc	= &proc_a;
	g_proc_table[10].m_priority		= MEDIUM;

	/* Add process B to the table as the 12th element */
	g_proc_table[11].m_pid			= (U32)(PID_B);
	g_proc_table[11].m_stack_size	= USR_SZ_STACK;
	g_proc_table[11].mpf_start_pc	= &proc_b;
	g_proc_table[11].m_priority		= MEDIUM;

	/* Add process C to the table as the 13th element */
	g_proc_table[12].m_pid			= (U32)(PID_C);
	g_proc_table[12].m_stack_size	= USR_SZ_STACK;
	g_proc_table[12].mpf_start_pc	= &proc_c;
	g_proc_table[12].m_priority		= MEDIUM;
	
	/* Add process clock timer to the table as the 14th element */
	g_proc_table[13].m_pid			= (U32)(PID_CLOCK);
	g_proc_table[13].m_stack_size	= USR_SZ_STACK;
	g_proc_table[13].mpf_start_pc	= &wall_clock_process;
	g_proc_table[13].m_priority		= HIGH;
	
//	g_proc_table[12].m_pid			= (U32)(PID_CLOCK);
//	g_proc_table[12].m_stack_size	= USR_SZ_STACK;
//	g_proc_table[12].mpf_start_pc	= &wall_clock_process;
//	g_proc_table[12].m_priority		= HIGH;
	
	/* initilize scheduler for each priority */
	for (i = 0; i < NUM_PRIORITIES; i++) {
		g_scheduler[i].head = NULL;
		g_scheduler[i].tail = NULL;
		g_scheduler[i].size = 0;
	}
	/* initilize blk by mem queue for each priority */
	for (i = 0; i < NUM_PRIORITIES; i++) {
		g_blked_mem_queue[i] = (queue){NULL, NULL, 0};
		#ifdef DEBUG_PROC
		printf("blk_mem_que[%02d], size %d, is null: %d \r\n", i,g_blked_mem_queue[i].size, dequeue(&g_blked_mem_queue[i]) == NULL);
		#endif
	}

	#ifdef DEBUG_PROC  
		printf("==================================\r\n");
		printf("Initializing g_proc_table...... \r\n");
	
	
		for ( i = 0; i < num+1; i++ ) {
			printf("==================================\r\n");
			printf("g_proc_table[%02d]: \r\n", i);		
			printf("g_proc_table[%02d].m_pid = %02d \r\n",i, g_proc_table[i].m_pid);
			printf("g_proc_table[%02d].mpf_start_pc = 0x%08x \r\n",i, g_proc_table[i].mpf_start_pc);
			printf("g_proc_table[%02d].m_priority = %02d \r\n",i, g_proc_table[i].m_priority);	
		}
	
		printf("==================================\r\n");
		printf("Initializing g_pcbs...... \r\n");
	#endif

	#ifdef DEBUG_0 
		printf("gp_pcbs in process = 0x%x \r\n",i, gp_pcbs);
	#endif
	
	/* initilize exception stack frame (i.e. initial context) for each process */
	for ( i = 0; i < num+8; i++ ) {
		int j;
		(gp_pcbs[i])->m_pid = (g_proc_table[i]).m_pid;
		(gp_pcbs[i])->m_state = NEW;
		(gp_pcbs[i])->m_priority = (g_proc_table[i]).m_priority;
		(gp_pcbs[i])->m_mem_given = NULL;
		if ((gp_pcbs[i])->m_pid == PID_CRT) {
			(gp_pcbs[i])->control_bit = 0;
		} else{
			(gp_pcbs[i])->control_bit = 1;
	  }
		(gp_pcbs[i])->mp_next = NULL;
		(gp_pcbs[i])->msg_queue = (queue){NULL, NULL, 0}; //no messege for any process initially
		/* Add all the (NEW) PCBs into the scheduler */
		enqueue(&g_scheduler[(gp_pcbs[i])->m_priority],(node*)gp_pcbs[i]);
		#ifdef DEBUG_0  
				printf("==================================\r\n");
				printf("gp_pcbs[%02d] in process = 0x%x, memory given is 0x%x \r\n",i, gp_pcbs[i],  gp_pcbs[i]->m_mem_given);
		#endif

#ifdef DEBUG_PROC  
	printf("==================================\r\n");
	printf("gp_pcbs[%02d]: \r\n", i);		
	printf("gp_pcbs[%02d]->m_pid = %02d \r\n",i, gp_pcbs[i]->m_pid);
	printf("gp_pcbs[%02d]->m_state = %02d \r\n",i, gp_pcbs[i]->m_state);
	printf("gp_pcbs[%02d]->m_priority = %02d \r\n",i, gp_pcbs[i]->m_priority);
#endif
		
		sp = alloc_stack((g_proc_table[i]).m_stack_size);
		*(--sp)  = INITIAL_xPSR;      // user process initial xPSR  
		*(--sp)  = (U32)((g_proc_table[i]).mpf_start_pc); // PC contains the entry point of the process
		for ( j = 0; j < 6; j++ ) { // R0-R3, R12 are cleared with 0
			*(--sp) = 0x0;
		}
		(gp_pcbs[i])->mp_sp = sp;
	}

#ifdef TIMER_IPROC
	/* Initialize the message records for both send and receive */
	send_msg_queue = k_request_memory_block();
	receive_msg_queue = k_request_memory_block();
  send_msg_queue->size = 0;
	send_msg_queue->first_record = 0;
	receive_msg_queue->size = 0;
	receive_msg_queue->first_record = 0;

	/* Timer i-proc initialization */
	// initialize timer_i_proc as the last element of gp_pcbs
	gp_pcb_timer_iproc = gp_pcbs[i];
	gp_pcb_timer_iproc->m_pid = PID_TIMER_IPROC;
	gp_pcb_timer_iproc->m_state = IPROC;
	gp_pcb_timer_iproc->m_mem_given = NULL;
	gp_pcb_timer_iproc->control_bit = 0;
	gp_pcb_timer_iproc->mp_next = NULL;	
  gp_pcb_timer_iproc->m_priority = HIGH;	
	gp_pcb_timer_iproc->msg_queue = (queue){NULL, NULL, 0}; //no messege initially
	gp_pcb_timer_iproc->mp_sp = alloc_stack(STACK_SIZE_IPROC);
	
	i++;
	/* UART i-proc initialization */
	gp_pcb_UART_iproc = gp_pcbs[i];
	gp_pcb_UART_iproc->m_pid = PID_UART_IPROC;
	gp_pcb_UART_iproc->m_state = IPROC;
	gp_pcb_UART_iproc->m_mem_given = NULL;
	gp_pcb_UART_iproc->control_bit = 0;
	gp_pcb_UART_iproc->mp_next = NULL;
	gp_pcb_UART_iproc->m_priority = HIGH;	
	gp_pcb_UART_iproc->msg_queue = (queue){ NULL, NULL, 0 }; //no messege initially
	gp_pcb_UART_iproc->mp_sp = alloc_stack(STACK_SIZE_IPROC);

	/* NOTE we do not need to create exception stack frame for an IPROC
	   since they are running in handler mode and never get into the handler
	   mode from the thread mode and they never exit from the handler mode
	   back to thread mode either
	*/
#endif
}

/**************************************************************************//**
 * @brief: Each Exception Stack should be initialized as follows:

		  +---------------------------+ High Address
		  |           ...             | <--- old sp
		  |---------------------------|
		  |       INITIAL_xPSR        |
		  |---------------------------|
		  |          PC (R15) = 0     |
		  |---------------------------|
		  |         LR (R14) = 0      |
		  |---------------------------|
		  |         R12 = 0           |
		  |---------------------------|
		  |         R3 = 0            |
		  |---------------------------|
		  |         R2 = 0            |
		  |---------------------------|
		  |         R1 = 0            |
		  |---------------------------|
		  |         R0 = 0            | <--- new sp
		  |---------------------------| 
		  |           ...             |
		  +---------------------------+ Low Address

*/


/**************************************************************************//**
 * @brief   scheduler, pick the PCB in RDY/NEW state with the highest 
 *			priority possible (FIFO).
 * @return  PCB pointer of the next to run process
 *          NULL if not possible
 * @post    if gp_current_process was NULL, then it gets set to the PCB in RDY/NEW 
 *			state with the highest priority possible
 *          No other effect on other global variables.
 *****************************************************************************/

PCB *scheduler(void)
{
#ifdef DEBUG_PROC  
	printf("==================================\r\n");
	printf("[DEBUG PROC] scheduler() is called. \r\n");
	printf("[DEBUG PROC] gp_current_process->pid = %02d \r\n", gp_current_process->m_pid);
	printf("[DEBUG PROC] gp_current_process->priority = %02d \r\n", gp_current_process->m_priority);
	printf("[DEBUG PROC] gp_current_process->state = %02d \r\n", gp_current_process->m_state);
#endif
	
	int priority;
	node* tmpNode;
	
	// Pick the PCB from the highest possible priorities (using dequeue)
	for (priority = HIGH; priority < NUM_PRIORITIES; priority++) {
		tmpNode = dequeue(&g_scheduler[priority]);

		// return the PCB if not NULL
		if (tmpNode != NULL) {
			#ifdef DEBUG_PROC  
				printf("[DEBUG PROC] scheduler() chose process %02d. \r\n", ((PCB*)tmpNode)->m_pid);
				printf("[DEBUG PROC] process %02d, priority=%02d. \r\n",((PCB*)tmpNode)->m_pid, ((PCB*)tmpNode)->m_priority);
			#endif
			return (PCB*)tmpNode;
		}
	}

	// Due to the existence of NULL process, this should never been reached
	return NULL;
}

/**************************************************************************//**
 * @brief   put current PCB (gp_current_process) in RDY mode and switch to a 
 *			new PCB (decided by scheduler()) and run it 
 * @return  RTX_OK upon success
 *          RTX_ERR upon failure
 * @pre     p_pcb_old and gp_current_process are pointing to valid PCBs.
 * @post    if gp_current_process was NULL, then it gets set to pcbs[0].
 *          No other effect on other global variables.
 *****************************************************************************/

int process_switch(void) 
{
#ifdef DEBUG_PROC  
	printf("==================================\r\n");
	printf("[DEBUG PROC] process_switch() is called. \r\n");
	printf("[DEBUG PROC] Current_process: pid = %02d, priority = %02d, state = %02d \r\n", gp_current_process->m_pid, gp_current_process->m_priority, gp_current_process->m_state);
#endif
	PROC_STATE_E state;
	PCB* old_process = NULL;
	PCB* new_process;

	// Set the state of PCB of current process node to RDY
	// This will also put the current_process to the scheduler (ready queue)
	if (NULL != gp_current_process) {
		// do not block timer or set it to any other state
		if (gp_current_process->m_pid == PID_TIMER_IPROC){
				return RTX_OK;
		}
		else if (gp_current_process->m_state != BLK_MEM && gp_current_process->m_state != BLK_MSG) {
			// 1. set current state to RDY
			// 2. put the current process to a ready queue
			if (RTX_ERR == k_set_process_state(gp_current_process->m_pid, gp_current_process, RDY)) {
			  return RTX_ERR;
		  }
		}

		// 3. Save current_process as old_process
		old_process = gp_current_process;
	}
	// Reset the flag for g_is_released to be true,
	g_is_released = true;

	// 4. scheduler dequeues a new RDY process
	// Get the PCB from scheduler (which is dequeued from a READY queue)
	new_process = scheduler();

	#ifdef DEBUG_PROC  
			printf("[DEBUG PROC] Back from scheduler to process_switch(). \r\n");
	#endif
	
	// 5. set new process to RUN state
	//new_process->m_state = RUN;

	// 6. Set the current_process to the new process
	gp_current_process = new_process;

	// 7. switch stacks of the old_process and current_process
	// Context Switching (Here comes the MSP and PSP stuff)
	state = gp_current_process->m_state;

	#ifdef DEBUG_PROC  
			printf("[DEBUG PROC] Current Process %02d has priority %02d. \r\n", gp_current_process->m_pid, gp_current_process->m_priority);
			printf("[DEBUG PROC] State : %02d and priority %02d. \r\n", gp_current_process->m_state, k_get_process_priority(gp_current_process->m_pid));
	#endif

	if (state == NEW) {
		if (old_process != NULL && gp_current_process != old_process && old_process->m_state != NEW) {
			old_process->mp_sp = (U32 *) __get_MSP();
		}
		gp_current_process->m_state = RUN;
	    __set_MSP((U32) gp_current_process->mp_sp);
		if (gp_current_process->control_bit == 1) {
			__set_CONTROL(__get_CONTROL() | BIT(0));
		} else {
			__set_CONTROL(__get_CONTROL() & (0xFFFFFFFF - BIT(0)));
		}
		__rte();
	} 

	/* The following will only execute if the if block above is FALSE */
	if (state == RDY){ 		
		old_process->mp_sp = (U32 *) __get_MSP(); // save the old process's sp
		gp_current_process->m_state = RUN;
		__set_MSP((U32) gp_current_process->mp_sp); //switch to the new proc's stack    
		if (gp_current_process->control_bit == 1) {
			__set_CONTROL(__get_CONTROL() | BIT(0));
		} else {
			__set_CONTROL(__get_CONTROL() & (0xFFFFFFFF - BIT(0)));
		}
	#ifdef DEBUG_PROC  
		printf("[DEBUG PROC] Process_switch: current process is RDY \r\n");
		printf("[DEBUG PROC] Current Process %02d has priority %02d. \r\n", gp_current_process->m_pid, gp_current_process->m_priority);
		printf("[DEBUG PROC] State : %02d and priority %02d. \r\n", gp_current_process->m_state, k_get_process_priority(gp_current_process->m_pid));
	#endif
	} else {
		#ifdef DEBUG_PROC  
			printf("==================================\r\n");
			printf("[DEBUG PROC] process_switch() The process we are about to run is not in RDY state. \r\n");
			printf("[DEBUG PROC] gp_current_process->pid = %02d \r\n", gp_current_process->m_pid);
			printf("[DEBUG PROC] gp_current_process->state = %02d \r\n", gp_current_process->m_priority);
			printf("[DEBUG PROC] gp_current_process->state = %02d \r\n", gp_current_process->m_state);
		#endif
		return RTX_ERR;
	} 

	return RTX_OK;
}

/**************************************************************************//**
 * @brief   release_processor() gives up the control of the processor and call 
 *			process_switch() (which inherently calls scheduler) to decide which
 *			process should be executed next
 * @return  RTX_OK upon success
 *          RTX_ERR upon failure
 * @post    gp_current_process gets updated to next to run process
 *****************************************************************************/

int k_release_processor(void)
{
#ifdef DEBUG_PROC  
	printf("==================================\r\n");
	printf("[DEBUG PROC] release_processor() is called. \r\n");
	printf("[DEBUG PROC] Current_process: pid = %02d, priority = %02d, state = %02d \r\n", gp_current_process->m_pid, gp_current_process->m_priority, gp_current_process->m_state);
#endif
	
	// The default value of global variable g_is_released is set to true.
 	return process_switch();
}

/**************************************************************************//**
 * @brief   set_process_priority().
 * @return  RTX_OK upon success
 *          RTX_ERR upon failure
 * @post    priority gets updated to next to run process
 *****************************************************************************/
int  k_set_process_priority(int pid, int priority) {
#ifdef DEBUG_PROC  
	printf("==================================\r\n");
	printf("[DEBUG PROC] set_priority() is called. \r\n");
	printf("[DEBUG PROC] Current_process: pid = %02d, priority = %02d, state = %02d \r\n", gp_current_process->m_pid, gp_current_process->m_priority, gp_current_process->m_state);
	printf("[DEBUG PROC] Set pid:%02d to priority:%02d \r\n", pid, priority);
#endif
	
	if (pid < PID_P1 || pid > PID_UART_IPROC ||
		priority < HIGH || priority > LOWEST) {
		return RTX_ERR;
	}

	int i;
	// find the PCB with corresponding pid 
	for (i = 1; i < NUM_PROCS; ++i) { /* The lower bound is to eliminate the chance to set priority for the null process. */
		if (pid == gp_pcbs[i]->m_pid) {
			// found the PCB, now set its priority

			// if the priority of the PCB is the same as what we want, we do nothing
			if (priority != gp_pcbs[i]->m_priority) {

				switch (gp_pcbs[i]->m_state) {
				case (RDY):
				case (NEW):
					// we modify the scheduler only if the process is in state RDY or NEW

					// remove the PCB from the scheduler
					if (RTX_ERR == queue_remove(&g_scheduler[gp_pcbs[i]->m_priority], (node*)gp_pcbs[i])) {
						return RTX_ERR;
					}

					// change the priority of the PCB in PCB list and add it back to the scheduler
					gp_pcbs[i]->m_priority = priority;

					if (RTX_ERR == enqueue(&g_scheduler[priority], (node*)gp_pcbs[i])) {
						return RTX_ERR;
					}

					#ifdef DEBUG_PROC  
						printf("==================================\r\n");
						printf("[DEBUG PROC] In function set_priority(). \r\n");
						printf("[DEBUG PROC] Current_process: pid = %02d, priority = %02d, get_priority = %02d \r\n", gp_current_process->m_pid, gp_current_process->m_priority, k_get_process_priority(gp_current_process->m_pid));
						printf("[DEBUG PROC] Target_process: pid = %02d, priority = %02d, get_priority = %02d \r\n", gp_pcbs[i]->m_pid, gp_pcbs[i]->m_priority, k_get_process_priority(gp_pcbs[i]->m_pid));
					
					#endif

					
					//	if the target priority is higher than the one for current process, call process_switch()
					if (priority < gp_current_process->m_priority) {
						g_is_released = false;
						process_switch();
					}

					break;
				case (RUN):
					// the process we are modifying is the current process

					// a further check
					if (!is_equal((node*)gp_pcbs[i], (node*)gp_current_process)) {
						#ifdef DEBUG_PROC  
						printf("==================================\r\n");
						printf("[DEBUG PROC] In function set_priority(). Unreachable code is reached! \r\n");
						printf("[DEBUG PROC] Error: A process which is not Current_process is in state RUN. \r\n");
						printf("[DEBUG PROC] Current_process: pid = %02d, priority = %02d, state = %02d \r\n", gp_current_process->m_pid, gp_current_process->m_priority, gp_current_process->m_state);
						printf("[DEBUG PROC] Target_process: pid = %02d, priority = %02d, state = %02d \r\n", gp_pcbs[i]->m_pid, gp_pcbs[i]->m_priority, gp_pcbs[i]->m_state);
						printf("[DEBUG PROC] Set pid:%02d to priority:%02d \r\n", pid, priority);
						#endif
						return RTX_ERR;
					}

					// if we are setting the priority of current process to a lower priority
					if (priority > gp_current_process->m_priority) {
					
						// set the priority
						gp_current_process->m_priority = priority;
						
						#ifdef DEBUG_PROC  
						printf("==================================\r\n");
						printf("[DEBUG PROC] In function set_priority(). \r\n");
						printf("[DEBUG PROC] Current process is setting its own priority to a lower one. \r\n");
						#endif		
						
						// if exists RDY/NEW process with higher or equal priority as the current process
						// The system should call process_switch
						for (i = 0; i <= priority; i++) {
							if (g_scheduler[i].size > 0) {
								// before calling process_switch, we should put the pcb into the priority queue
								gp_current_process->m_state = RDY;
								if (RTX_ERR == enqueue(&g_scheduler[priority], (node*)gp_current_process)) {
									return RTX_ERR;
								}
								process_switch();
								break;
							}
						}
					}else{
						// set the priority
						gp_current_process->m_priority = priority;
					}
					break;
				case (BLK_MEM):
					// we modify the blked_mem_queue only if the process is in state BLK_MEM

					// remove the PCB from the blked_mem_queue
					if (RTX_ERR == queue_remove(&g_blked_mem_queue[gp_pcbs[i]->m_priority], (node*)gp_pcbs[i])) {
						return RTX_ERR;
					}

					// change the priority of the PCB in PCB list and add it back to the scheduler
					gp_pcbs[i]->m_priority = priority;

					if (RTX_ERR == enqueue(&g_blked_mem_queue[priority], (node*)gp_pcbs[i])) {
						return RTX_ERR;
					}
					break;
				case (BLK_MSG):
					//if process is blocked, then changing its priority shouldn't preempt anything
					//neither is it in any queue
					gp_pcbs[i]->m_priority = priority;
				}
			}
			return RTX_OK;
		}
	}
	return RTX_ERR;
}

/**************************************************************************//**
 * @brief   get_process_priority().
 * @return  The expected priority of the pid upon success
 *          RTX_ERR upon failure
 * @post    gp_current_process gets updated to next to run process
 *****************************************************************************/
int  k_get_process_priority(int pid) {
#ifdef DEBUG_PROC  
	printf("==================================\r\n");
	printf("[DEBUG PROC] get_priority() is called. \r\n");
	printf("[DEBUG PROC] Current_process: pid = %02d, priority = %02d, state = %02d \r\n", gp_current_process->m_pid, gp_current_process->m_priority, gp_current_process->m_state);
	printf("[DEBUG PROC] Get pid:%02d\r\n", pid);
#endif
	int i;
	// find the PCB with corresponding pid and return its priority
	for (i = 0; i < NUM_PROCS; ++i) {
		if (pid == gp_pcbs[i]->m_pid) {
			return gp_pcbs[i]->m_priority;
		}
	}
	return RTX_ERR;
}

/**************************************************************************//**
 * @brief   set_process_state().
 * @return  RTX_OK upon success
 *          RTX_ERR upon failure
 * @post    process with expected pid gets updated to the right state
 *			the scheduler and blked_mem_queue will get updated
 *****************************************************************************/
int  k_set_process_state(int pid, PCB* pcb, int state) {
#ifdef DEBUG_PROC  
	printf("==================================\r\n");
	printf("[DEBUG PROC] set_process_state() is called. \r\n");
	printf("[DEBUG PROC] Current_process: pid = %02d, priority = %02d, state = %02d \r\n", gp_current_process->m_pid, gp_current_process->m_priority, gp_current_process->m_state);
	if(NULL != pcb){
		printf("[DEBUG PROC] Set pid:%02d / PCB:0x%08x to state:%02d \r\n", pcb->m_pid ,pcb,state);		
	}else{
		printf("[DEBUG PROC] Set pid:%02d to state:%02d \r\n", pcb->m_pid ,pcb,state);				
	}
#endif
	int i;

	// find the PCB with corresponding pid if the input PCB is NULL
	if (NULL == pcb) {
		for (i = 0; i < NUM_PROCS; ++i) {
			if (pid == gp_pcbs[i]->m_pid) {
				pcb = gp_pcbs[i];
			}
		}
		// return Error if the pid cannot be found
		return RTX_ERR;
	}

	// if the new state is the same, do nothing
	if (state != pcb->m_state) {

		// ----------     decision based on the original state     ----------
		
		// if the original state was BLK_MEM (exists in the blked_mem_queue), then remove it
		if (BLK_MEM == pcb->m_state) {
			if (RTX_ERR == queue_remove(&g_blked_mem_queue[pcb->m_priority], (node*)pcb)) {
				return RTX_ERR;
			}
		}
		
		// set the state of the PCB
		pcb->m_state = (PROC_STATE_E)state;

		// ----------     decision based on the target state     ----------

		// if the target state is RDY, then add it to the scheduler
		if (RDY == state) {

			// if the current process released processor **volunteerly**, we put the PCB at the back of the PQ
			if (g_is_released) {
				if (RTX_ERR == enqueue(&g_scheduler[pcb->m_priority], (node*)pcb)) {
					return RTX_ERR;
				}
			}
			// else, we put the PCB at the front of the PQ
			else {
				if (RTX_ERR == enqueue_opposite(&g_scheduler[pcb->m_priority], (node*)pcb)) {
					return RTX_ERR;
				}
			}

			// if the PCB has a higher priority than the current process (and it's ready), then call process_switch()
			if (pcb->m_priority < gp_current_process->m_priority) {
				g_is_released = false;
				process_switch();
			}
		}

		// if the target state is BLK_MEM, then add it to the blked_mem_queue
		if (BLK_MEM == state) {
			if (RTX_ERR == enqueue(&g_blked_mem_queue[pcb->m_priority], (node*)pcb)) {
				return RTX_ERR;
			}
		}
	}
	return RTX_OK;
}

/**************************************************************************//**
 * @brief   run a new process based on the scheduler()
			This is very similar to release_processor()
 * @return  RTX_ERR on error and zero on success
 * @post    gp_current_process gets updated to next to run process
 *****************************************************************************/
int k_run_new_process(void)
{
#ifdef DEBUG_TIMER
	//printf("k_run_new_process: %d \r\n", gp_current_process == NULL);
#endif
	
	if (gp_current_process == NULL) {
		return RTX_ERR;
	}
	
	// if exists RDY/NEW process with higher priority as the current process
	// The system should call process_switch
	for (int i = 0; i < gp_current_process->m_priority; i++) {
		if (g_scheduler[i].size > 0) {
			// before calling process_switch, we should put the pcb into the priority queue
			// enqueue_opposite to put it as the head
			gp_current_process->m_state = RDY;
			if (RTX_ERR == enqueue_opposite(&g_scheduler[gp_current_process->m_priority], (node*)gp_current_process)) {
				return RTX_ERR;
			}
			return process_switch();
		}
	}
	
	return RTX_OK;
}

/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
