/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTX LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *                          All rights reserved.
 *---------------------------------------------------------------------------
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice and the following disclaimer.
 *
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
/**************************************************************************//**
 * @brief   Memory preemption test 2
 * @author  Jianyan Li
 * @date    2020/03/16
 * @note    Each process is in an infinite loop. Processes never terminate.
 *****************************************************************************/

/*------------------------------------------------------------------------*
expected output at COM1 (polled terminal) 
---------------------------------------------------------------------------
proc1: entering...
proc1: try to get message
proc2: entering...
proc2: send msg to P1
proc1: unblocked by 2
proc1: set my priority to lowest
proc2: running again
--------------------------------------------------------------------------
NOTE: the tm.sec and tm.nsec most likely will not match your output.
      the time absolute error tolerance is 1 ms, 
      which is the granularity of the timer0 interrupt frequency.
-------------------------------------------------------------------------*/

#include "ae_proc.h"
#include "ae.h"
#include "uart_polling.h"
#include "printf.h"

/* initialization table item */
void set_test_procs(PROC_INIT *procs, int num)
{
    int i;
    for( i = 0; i < num; i++ ) {
        procs[i].m_pid        = (U32)(i+1);
        procs[i].m_stack_size = 0x300;
    }
  
    procs[0].mpf_start_pc = &proc1;
    procs[0].m_priority   = HIGH;
    
    procs[1].mpf_start_pc = &proc2;
    procs[1].m_priority   = LOW;
    
    procs[2].mpf_start_pc = &proc3;
    procs[2].m_priority   = LOW;
    
    procs[3].mpf_start_pc = &proc4;
    procs[3].m_priority   = LOWEST; 
    
    procs[4].mpf_start_pc = &proc5;
    procs[4].m_priority   = LOWEST;
    
    procs[5].mpf_start_pc = &proc6;
    procs[5].m_priority   = LOWEST;
    
}

/**
 * @brief process 1 is blocked by msg, then unblocked after P2 sends 
 * the msg, then set priority of itself to lowest
 */
void proc1(void)
{
    MSG_BUF *p_msg;
    int     sender_id;
    
    uart1_put_string("proc1: entering...\n\r");
    p_msg = (MSG_BUF *)request_memory_block();
		uart1_put_string("proc1: try to get message\n\r");
    p_msg = receive_message(&sender_id);
    printf("proc1: unblocked by %d ...\n\r", sender_id);
		printf("proc1: set my priority to lowest\n\r", sender_id);
    set_process_priority(PID_P1, LOWEST);
    while (1) {
        uart1_put_string("proc1:\n\r");
        release_processor();
    }
}

/**
 * @brief process 2 ublocks P1, then gets preempted by P1, 
 * after P1 is done, P2 takes over
 */

void proc2(void)
{
    int     retval;
    
    uart1_put_string("proc2: entering...\n\r");
    MSG_BUF* p_msg_env = (MSG_BUF*) request_memory_block();
		p_msg_env->mtype = KCD_CMD;
		p_msg_env->mtext[0] = '%';
	  uart1_put_string("proc2: send msg to P1\n\r");
		retval = send_message(1, (void*) p_msg_env);//should preempt P1 -> p2 at back of queue
    uart1_put_string("proc2: running again\n\r");

    while(1) {
        //uart1_put_string("proc2:\n\r");
        release_processor();
    }
}

void proc3(void)
{
    uart1_put_string("proc3:\n\r");
    while(1) {
        release_processor();
    }
}

void proc4(void)
{
    uart1_put_string("proc4:\n\r");
    while(1) {
        release_processor();
    }
}

void proc5(void)
{
    while(1) {
        uart1_put_string("proc5:\n\r");
        release_processor();
    }
}

void proc6(void)
{
    while(1) {
        uart1_put_string("proc6:\n\r");
        release_processor();
    }
}