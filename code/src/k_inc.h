/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTX LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *                          All rights reserved.
 *---------------------------------------------------------------------------
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice and the following disclaimer.
 *
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
 

/**************************************************************************//**
 * @file        k_inc.h
 * @brief       Kernel Macros and Data Structure Header file          
 * @version     V1.2021.01
 * @authors     Yiqing Huang
 * @date        2021 JAN
 *
 * @note        all kernel .c files include this one
 *
 *****************************************************************************/
 
#ifndef K_INC_H_
#define K_INC_H_
#include <LPC17xx.h>
#include <system_LPC17xx.h>
#include "common.h"
#include "queue.h"

#ifdef DEBUG_0
#include "uart_polling.h"
#include "printf.h"
#endif /* DEBUG_0 */

 /*
  *===========================================================================
  *                             MACROS
  *===========================================================================
  */

/* command related */
#define MAX_CMD_SIZE 64

/* process states, note we assume four states */
typedef enum {NEW = 0, RDY, RUN, BLK_MEM, BLK_MSG, IPROC} PROC_STATE_E;  

/**
 * @brief PCB data structure definition.
 * @note  You will need to add your own member variables
 *         in order to finish the project 
 */
typedef struct pcb 
{ 
    struct pcb   *mp_next;      /**> next pcb, not used i 1n this example      */  
    U32          m_pid;         /**> process id, 4B offset                   */
    U32          m_priority;    /**> process priority                        */
    PROC_STATE_E m_state;       /**> state of the process 12B offset          */      
    U32          *mp_sp;        /**> stack pointer of the process, 16B offset */
    void*		 m_mem_given;   /**> memory freed up by release_memory()*/
    BOOL		control_bit; 	/**> Unpriviledged = 1, otherwise = 0   */
    queue		msg_queue;     /**> Queue for messeges of this process */
} PCB;



 /* ATTENTION: These OFFSET macros depends on the PCB struct
  *            member field location in the struct.
  *            You need to update them accordingly
  *             if you make changes of PCB structure.
  */
#define PCB_MSP_OFFSET      16       /* mp_sp is 16B offset from pcb struct starting addr.    */
#define PCB_STATE_OFFSET    12      /* m_state is 12B offset from pcb struct starting addr. */
#define STACK_SIZE_IPROC    USR_SZ_STACK   /* iprocess stack size */

#ifdef TIMER_IPROC
    #define NUM_PROCS (NUM_TEST_PROCS + 1 + 5 + 4)	/* Total number of processes (6 test proc + null proc + timer related processes ) */
    /* Timer related processes */
    /* (5) TIMER0_I + KCD + UART0_I + CRT + SET_PRIORITY */
    /* Stress test processes */
    /* (3) A + B + C */
#else
    #define NUM_PROCS (NUM_TEST_PROCS + 1)	/* Total number of processes (6 test proc + null proc) */
#endif /* TIMER_IPROC */

/*
 *==========================================================================
 *                   GLOBAL VARIABLES DECLARATIONS
 *==========================================================================
 */
/* This symbol is defined in the scatter file (see RVCT Linker User Guide)*/  
extern unsigned int Image$$RW_IRAM1$$ZI$$Limit; 
extern U32          *gp_stack;            /* see k_memory.c for details   */  
extern PCB          **gp_pcbs;            /* array of pcbs */
extern PCB          *gp_current_process;  /* the current RUN process      */
extern PCB          *gp_pcb_timer_iproc;  /* points to Timer iprocess pcb */
extern PCB          *gp_pcb_UART_iproc;   /* points to UART iprocess pcb */
extern PCB          *gp_pcb_interrupted;  /* interrupted process's pcb    */
extern PROC_INIT    g_proc_table[NUM_PROCS];
                                          /* process initialization table */

extern volatile uint32_t g_timer_count;   /* increment every 1 ms         */

#endif /* ! K_INC_H_ */
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
