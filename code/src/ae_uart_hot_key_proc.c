/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTOS LAB
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *
 *          This software is subject to an open source license and
 *          may be freely redistributed under the terms of MIT License.
 ****************************************************************************
 */

 /**************************************************************************//**
  * @file        ae_uart_hot_key_proc.c
  * @brief       Used to test the correctness of delayed_send
  *
  * @version     V1.2021.03
  * @authors     KW
  * @date        2021 March
  * @note        Each process is in an infinite loop. Processes never terminate.
  * @details
  *
  *****************************************************************************/
  /*----------------------------------------------------------------------------
   * Hot-key tests:
   * Note: in order to test this test case, the user should type into the console to see the result
   * Testing steps:

  1. Wait for the polling terminal to print out the following line
				"G005_test: Ready for input."
  2. Press '!'. Expected result:
				[UART0] Processes are Ready:
				PID: 1. Priority: 1.
				PID: 2. Priority: 1.
				PID: 3. Priority: 2.
				[UART0] End.
  3. Press '@'. Expected result:
				[UART0] Processes are Blocked on Memory:
				PID: 4. Priority: 0.
				[UART0] End.
  4. Press #. Expected result:
				[UART0] Processes are blocked on Receive:
				PID: 5. Priority: 1.
				PID: 12. Priority: 0.
				[UART0] End.
  5. Press '$'. Expected result (We might have more processes):
				[UART0] All processes priority and states:
				PID: 0. Priority: 4. State: NEW.
				PID: 1. Priority: 1. State: RDY.
				PID: 2. Priority: 1. State: RDY.
				PID: 3. Priority: 2. State: RDY.
				PID: 4. Priority: 0. State: BLK_MEM.
				PID: 5. Priority: 1. State: BLK_MSG.
				PID: 6. Priority: 0. State: RUN.
				PID: 12. Priority: 0. State: BLK_MSG.
				PID: 14. Priority: 0. State: IPROC.
				PID: 15. Priority: 0. State: IPROC.
				[UART0] End.
  6. Press '&'. Expected result:
				[UART0] Available memory block: 0.
				[UART0] End.
  7. Press '^'. Expected result (message preview might be different):
				[UART0] 10 most recent IPC Send:
				1. Sender:1. Receiver:2. Message type:1. Time stamp:0. Message preview:
				2. Sender:1. Receiver:2. Message type:2. Time stamp:0. Message preview:
				3. Sender:1. Receiver:2. Message type:255. Time stamp:0. Message preview: ?
				4. Sender:1. Receiver:6. Message type:3. Time stamp:0. Message preview: �V
				5. Sender:1. Receiver:1. Message type:4. Time stamp:0. Message preview:�VV
				6. Sender:1. Receiver:1. Message type:5. Time stamp:0. Message preview:VV=
				7. Sender:1. Receiver:1. Message type:6. Time stamp:0. Message preview:V=?
				8. Sender:1. Receiver:1. Message type:7. Time stamp:0. Message preview:=?
				9. Sender:1. Receiver:1. Message type:8. Time stamp:0. Message preview:??
				10. Sender:5. Receiver:2. Message type:0. Time stamp:101. Message preview:

				[UART0] 10 most recent IPC Receive:
				1. Sender:1. Receiver:6. Message type:3. Time stamp:1. Message preview:,?
				2. Sender:1. Receiver:1. Message type:4. Time stamp:1. Message preview:,��
				3. Sender:1. Receiver:1. Message type:5. Time stamp:1. Message preview:��?
				4. Sender:1. Receiver:1. Message type:6. Time stamp:1. Message preview:��
				5. Sender:1. Receiver:1. Message type:7. Time stamp:1. Message preview:��
				6. Sender:1. Receiver:1. Message type:8. Time stamp:1. Message preview:�E
				7. Sender:1. Receiver:1. Message type:4. Time stamp:101. Message preview:�E?
				8. Sender:1. Receiver:5. Message type:0. Time stamp:101. Message preview:
				9. Sender:1. Receiver:1. Message type:5. Time stamp:121. Message preview:
				10. Sender:1. Receiver:6. Message type:3. Time stamp:501. Message preview:,
				[UART0] End.

		
   * Several test cases:
    1. Ready processes and their priorities
    2. blocked on memory processes and their priorities
    3. blocked on receive processes and their priorities
    4. all processes�� priorities and states
    5. number of memory blocks available
    6. 10 most recent IPC messages


   *---------------------------------------------------------------------------*/
#include "rtx.h"
#include "uart_polling.h"
#include "ae_proc.h"
#include "k_msg.h"
#include "printf.h"

// this very AE needs to use the hot keys debugging preprocessor
#ifndef  _DEBUG_HOTKEYS
#define _DEBUG_HOTKEYS
#endif // ! _DEBUG_HOTKEYS

/**************************************************************************//**
 * @brief:    Original priority: HIGH
 * @Purpose:  RDY
 *		 
 *****************************************************************************/
void proc1(void)
{
#ifdef DEBUG_0
	printf("[Process 1] Start executing process 1 \n");
	printf("[Process 1] process 1 priority = %02d \n", get_process_priority(PID_P1));
#endif /* DEBUG_0 */

	MSG_BUF* envlope = request_memory_block();
	envlope->mtype = 0;
	*envlope->mtext = 1;
	delayed_send(PID_P5, envlope,100);

	envlope = request_memory_block();
	envlope->mtype = 1;
	*envlope->mtext = 32;
	send_message(PID_P2, envlope);

	envlope = request_memory_block();
	envlope->mtype = 2;
	*envlope->mtext = 61;
	delayed_send(PID_P2, envlope, 20);

	envlope = request_memory_block();
	envlope->mtype = -1;
	*envlope->mtext = 72;
	delayed_send(PID_P2, envlope, 40);

	envlope = request_memory_block();
	envlope->mtype = 3;
	*envlope->mtext = 22;
	delayed_send(PID_P6, envlope, 500);

	envlope = request_memory_block();
	envlope->mtype = 4;
	*envlope->mtext = 33;
	delayed_send(PID_P1, envlope, 100);
	
	envlope = request_memory_block();
	envlope->mtype = 5;
	*envlope->mtext = 44;
	delayed_send(PID_P1, envlope, 120);
	
	envlope = request_memory_block();
	envlope->mtype = 6;
	*envlope->mtext = 55;
	delayed_send(PID_P1, envlope, 1000000);
	
	envlope = request_memory_block();
	envlope->mtype = 7;
	*envlope->mtext = 10;
	delayed_send(PID_P1, envlope, 1000000);
	
	envlope = request_memory_block();
	envlope->mtype = 8;
	*envlope->mtext = 100;
	delayed_send(PID_P1, envlope, 1000000);
	

	int sender_id;
	
	envlope = receive_message(&sender_id);
	envlope = receive_message(&sender_id);

	set_process_priority(PID_P4, HIGH);
	set_process_priority(PID_P2, MEDIUM);
	set_process_priority(PID_P1, MEDIUM);

	envlope = receive_message(&sender_id);
	envlope = receive_message(&sender_id);
	envlope = receive_message(&sender_id);

	while (1) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief:    Original priority: LOW
 * @Purpose:  RDY
 *
 *****************************************************************************/

void proc2(void)
{
	set_process_priority(PID_P6, HIGH);
	while (1) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief:    Original priority: LOW
 * @Purpose:  RDY
 *
 *****************************************************************************/

void proc3(void)
{

	while (1) {
		if(get_process_priority(PID_P3) == LOWEST){
			// meaning we reached the end of the test process
			break;
		}
		release_processor();
	}
	
	while (1) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief:    Original priority: LOWEST
 * @Purpose:  Blocked on Memory
 *
 *****************************************************************************/

void proc4(void)
{
	// this process will be changed to MEDIUM and starting to drain the mem_pool
	while (1) {
		MSG_BUF* envlope = request_memory_block();
	}
}

/**************************************************************************//**
 * @brief:    Original priority: MEDIUM
 * @Purpose:  Blocked on Receive
 *
 *****************************************************************************/

void proc5(void)
{
	int sender_id;
	void* env = receive_message(&sender_id);
	// transmit this message to PID_P2
	send_message(PID_P2, env);

	// no message is sent to PID5, hence, it will always be blocked on receiving
	env = receive_message(&sender_id);
}

/**************************************************************************//**
 * @brief:    Original priority: MEDIUM
 * @Purpose:  Tell the user the initialization is finished
 *
 *****************************************************************************/

void proc6(void)
{
	int sender_id;
	// blocked on message for a while
	void* env = receive_message(&sender_id);
	uart1_put_string("G005_test: Ready for input.\n\r");
	while (1) {
		release_processor();
	}
}
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
