/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTOS LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *
 *          This software is subject to an open source license and 
 *          may be freely redistributed under the terms of MIT License.
 ****************************************************************************
 */

/**************************************************************************//**
 * @file        ae_proc_mem_priviledge_test.c
 * @brief       test memory api and previledge issues
 *							expect output: test 1 -> 2 -> 3 -> 4
 *              
 * @version     V1.2021.02
 * @authors     JL
 * @date        2021 Feb
 * @note        Each process is in an infinite loop. Processes never terminate.
 * @details
 *
 *****************************************************************************/

#include "rtx.h"
#include "uart_polling.h"
#include "ae_proc.h"
#include "printf.h"
#include <LPC17xx.h>

int ok_counter = 0;
int total_test = 8;
/**************************************************************************//**
 * @brief: a process that tests send_message illegal value
 *****************************************************************************/
void proc1(void)
{	
	int retval;
	MSG_BUF* envlope;
	uart1_put_string("G005_test: START\r\n"); 
	printf("G005_test: total %d tests\r\n", total_test); 
	envlope = request_memory_block();
	retval = send_message(PID_P3, (void*) envlope);
	// test 1: envlope is a valid memory block
	if (retval == RTX_OK) {
			uart1_put_string("G005_test: test 1 OK\r\n");
			ok_counter++;
	} else {
			uart1_put_string("G005_test: test 1 FAIL\n\r");
	}
	
	retval = send_message(PID_P3, (void*) envlope);
	// test 2: proc does not own envlops
	if (retval == RTX_ERR) {
			uart1_put_string("G005_test: test 2 OK\r\n");
			ok_counter++;
	} else {
			uart1_put_string("G005_test: test 2 FAIL\n\r");
	}
	
	retval = send_message(PID_P3, (void*) NULL);
	// test 3: envelope is null
	if (retval == RTX_ERR) {
			uart1_put_string("G005_test: test 3 OK\r\n");
			ok_counter++;
	} else {
			uart1_put_string("G005_test: test 3 FAIL\n\r");
	}
	
	envlope = request_memory_block();
	retval = send_message(PID_P3, (void*) ((U8*)envlope + 4));
	// test 3: envelope has offset
	if (retval == RTX_ERR) {
			uart1_put_string("G005_test: test 4 OK\r\n");
			ok_counter++;
	} else {
			uart1_put_string("G005_test: test 4 FAIL\n\r");
	}
	
	envlope = request_memory_block();
	retval = delayed_send(PID_P3, (void*) envlope, 500);
	// test 2-1: envlope is a valid memory block
	if (retval == RTX_OK) {
			uart1_put_string("G005_test: test 5 OK\r\n");
			ok_counter++;
	} else {
			uart1_put_string("G005_test: test 5 FAIL\n\r");
	}
	
	retval = delayed_send(PID_P3, (void*) envlope, 500);
	// test 2-2: proc does not own envlops
	if (retval == RTX_ERR) {
			uart1_put_string("G005_test: test 6 OK\r\n");
			ok_counter++;
	} else {
			uart1_put_string("G005_test: test 6 FAIL\n\r");
	}
	
	retval = delayed_send(PID_P3, (void*) NULL, 500);
	// test 2-3: envelope is null
	if (retval == RTX_ERR) {
			uart1_put_string("G005_test: test 7 OK\r\n");
			ok_counter++;
	} else {
			uart1_put_string("G005_test: test 7 FAIL\n\r");
	}
	
	envlope = request_memory_block();
	retval = delayed_send(PID_P3, (void*) ((U8*)envlope + 4), 500);
	// test 2-4: envelope has offset
	if (retval == RTX_ERR) {
			uart1_put_string("G005_test: test 8 OK\r\n");
			ok_counter++;
	} else {
			uart1_put_string("G005_test: test 8 FAIL\n\r");
	}
	printf("G005_test: %d/%d test OK\n\r", ok_counter, total_test);
	printf("G005_test: %d/%d test FAIL\n\r", total_test - ok_counter, total_test);
	printf("G005_test: END\n\r");
	while ( 1 ) {
		release_processor();
	}
}

/**************************************************************************//**
 * @brief  a process that does nothing.
 *****************************************************************************/
void proc2(void)
{
	while ( 1 ) {	
		release_processor();
	}
}

/**************************************************************************//**
 * @brief  a process that unblocks p3 but is not preempted.
 *****************************************************************************/

void proc3(void)
{

    while(1) {
        release_processor();
    }
}

void proc4(void)
{
    while(1) {
        uart1_put_string("proc4: \n\r");
        release_processor();
    }
}

void proc5(void)
{
    while(1) {
        uart1_put_string("proc5: \n\r");
        release_processor();
    }
}

void proc6(void)
{
    while(1) {
        uart1_put_string("proc6: \n\r");
        release_processor();
    }
}
/*
 *===========================================================================
 *                             END OF FILE
 *===========================================================================
 */
