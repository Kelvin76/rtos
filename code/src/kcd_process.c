#include "kcd_process.h"
#include "rtx.h"
#include "uart_polling.h"
#include "k_inc.h"
#include "string.h"
/**************************************************************************//**
 * @brief  The KCD process (preemptive), which has the HIGH priority
 *         runs when unblocked (UART I proc sens messge to it).
					 Should keep all system processes at HIGH priority so that
					 they run in time.
 *****************************************************************************/

void	kcd_proc(void) {
	int send_id;
  MSG_BUF *p_msg;
	MSG_BUF* p_tmp_msg;
	short* cmd_mapping;
	int i; //tmp counter variable
	short i_s; //tmp short
	void *p_blk;
	#ifdef DEBUG_KCD
  char    *ptr;
	#endif
	char* cmd_buffer;
	int cmd_buffer_idx = 0;
	BOOL is_msg_transferred;
	char key_in;

	cmd_mapping = (short*)request_memory_block();//for storing mapping from cmd to pid
	cmd_buffer = (char*)request_memory_block();//for storing command
	//init cmd mapping to -1
	for (i = 0; i < 52; i++) {
		cmd_mapping[i] = -1;
	}
	while (TRUE) {
		p_msg = receive_message(&send_id);
		is_msg_transferred = false;
		if (p_msg->mtype == KCD_REG) {
			//register command
			//check format
			i_s = ascii_to_idx(p_msg->mtext[1]);
			if (p_msg->mtext[0] != '%' || 
					p_msg->mtext[2] != '\0' || 
					i_s < 0 ||
					send_id < PID_NULL ||
					send_id > PID_UART_IPROC) {
				uart1_put_string("KCD Error: cmd registration invalid\r\n");
				#ifdef DEBUG_KCD
				p_blk = request_memory_block();
				p_tmp_msg = p_blk;
				p_tmp_msg->mtype = CRT_DISPLAY;
				str_cpy(p_tmp_msg->mtext, "KCD Error: cmd registration invalid\r\n");
				send_message(PID_P2, p_blk);
				#endif
			} else {
				//register command
				cmd_mapping[i_s] = send_id;
			}
		}
		#ifdef DEBUG_KCD
		else if (p_msg->mtype == KEY_IN) {
		#else
		else if (p_msg->mtype == KEY_IN && send_id == PID_UART_IPROC) {
		#endif
			//command
			//cache key in 
			key_in = p_msg->mtext[0];
			//first forward to CRT
			p_msg->mtype = CRT_DISPLAY;
			is_msg_transferred = send_message(PID_CRT, p_msg) == RTX_OK;
		
			#ifdef DEBUG_KCD
			p_blk = request_memory_block();
			p_tmp_msg = p_blk;
			p_tmp_msg->mtype = CRT_DISPLAY;
			ptr = p_tmp_msg->mtext;
			*ptr++ = p_msg->mtext[0];
			*ptr++ = '\0';
			send_message(PID_P2, p_blk);
			#endif
			
			//queue command in buffer
			if (cmd_buffer_idx < MAX_CMD_SIZE) {
				cmd_buffer[cmd_buffer_idx] = p_msg->mtext[0];
			}
			cmd_buffer_idx++;
			//then check key
			if (key_in == '\r') {
				//if new line, commond input is complete
				//send a newline character to crt for proper display of new line
				p_blk = request_memory_block();
				p_tmp_msg = p_blk;
				p_tmp_msg->mtype = CRT_DISPLAY;
				p_tmp_msg->mtext[0] = '\n';
				p_tmp_msg->mtext[1] = '\0';
				send_message(PID_CRT, p_blk);
				//validate command
				i_s = ascii_to_idx(cmd_buffer[1]);
				if (i_s < 0 || i_s >= 52 || cmd_mapping[i_s] < 0 
						|| cmd_buffer[0] != '%' || cmd_buffer_idx > MAX_CMD_SIZE) {
					//If the command identifier is not registered
					//log error
					p_blk = request_memory_block();
					p_tmp_msg = p_blk;
					p_tmp_msg->mtype = CRT_DISPLAY;
							
					if (cmd_buffer[0] != '%' || cmd_buffer_idx > MAX_CMD_SIZE) {
						str_cpy(p_tmp_msg->mtext, "Invalid command\r\n");
						uart1_put_string("Invalid command\r\n");
						send_message(PID_CRT, p_blk);
						#ifdef DEBUG_KCD
						p_blk = request_memory_block();
						p_tmp_msg = p_blk;
						p_tmp_msg->mtype = CRT_DISPLAY;
						str_cpy(p_tmp_msg->mtext, "Invalid command\r\n");
						send_message(PID_P2, p_blk);
						#endif
					} else {
						str_cpy(p_tmp_msg->mtext, "Command not found\r\n");
						uart1_put_string("Command not found\r\n");
						send_message(PID_CRT, p_blk);
						#ifdef DEBUG_KCD
						p_blk = request_memory_block();
						p_tmp_msg = p_blk;
						p_tmp_msg->mtype = CRT_DISPLAY;
						str_cpy(p_tmp_msg->mtext, "Command not found\r\n");
						send_message(PID_P2, p_blk);
						#endif
					}
					
					//ignore string
					cmd_buffer_idx = 0;	
				} else {
					//send command
					p_blk = request_memory_block();
					p_tmp_msg = p_blk;
					p_tmp_msg->mtype = KCD_CMD;
					for (i = 0; i < cmd_buffer_idx; i++) {
						p_tmp_msg->mtext[i] = cmd_buffer[i];
					}
					//reset command buffer idx
					cmd_buffer_idx = 0;
					send_message(cmd_mapping[i_s], p_blk);	
				}
			}
		} 
		else {
			//log error
			uart1_put_string("KCD Error: msg type invalid\r\n");
			#ifdef DEBUG_KCD
			p_blk = request_memory_block();
			p_tmp_msg = p_blk;
			p_tmp_msg->mtype = CRT_DISPLAY;
			str_cpy(p_tmp_msg->mtext, "KCD Error: msg type invalid\r\n");
			send_message(PID_P2, p_blk);
			#endif
		}
		//release memory if we didn't transfer it to crt
		if (!is_msg_transferred) {
			release_memory_block((void*) p_msg);
		}
	}
}
