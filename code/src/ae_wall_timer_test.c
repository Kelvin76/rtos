

#include "ae_proc.h"
#include "ae.h"
#include "uart_polling.h"
#include "printf.h"

int ok_counter = 0;
int total_test = 11;

/* initialization table item */
void set_test_procs(PROC_INIT *procs, int num)
{
    int i;
    for( i = 0; i < num; i++ ) {
        procs[i].m_pid        = (U32)(i+1);
        procs[i].m_stack_size = 0x300;
    }
  
    procs[0].mpf_start_pc = &proc1;
    procs[0].m_priority   = HIGH;
    
//    procs[1].mpf_start_pc = &proc2;
//    procs[1].m_priority   = MEDIUM;
    
//    procs[2].mpf_start_pc = &proc3;
//    procs[2].m_priority   = LOW;
    
//    procs[3].mpf_start_pc = &proc4;
//    procs[3].m_priority   = LOWEST; 
    
//    procs[4].mpf_start_pc = &proc5;
//    procs[4].m_priority   = LOWEST;
    
//    procs[5].mpf_start_pc = &proc6;
 //   procs[5].m_priority   = LOWEST;
    
}

/**
 * @brief process 1 pretends to be UART I proc to 
		send key strokes to KCD
 */
void proc1(void)
{
    MSG_BUF *p_msg;
    int     sender_id;
		int i;
    
    uart1_put_string("proc1: entering...\n\r");
		//pretending to be a process to register command to KCD
    p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KCD_REG;
		p_msg->mtext[0] = '%';
		p_msg->mtext[1] = 'z';
		p_msg->mtext[2] = '\0';
    send_message(PID_KCD, (void *) p_msg);
	
		//pretending to be UART I, sending key stroke to kcd
    p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '%';
    send_message(PID_KCD, (void *) p_msg);

    p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = 'z';
    send_message(PID_KCD, (void *) p_msg);
		
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = ' ';
    send_message(PID_KCD, (void *) p_msg);
		
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = 'a';
    send_message(PID_KCD, (void *) p_msg);
		
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '.';
    send_message(PID_KCD, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '\n';
    send_message(PID_KCD, (void *) p_msg);
		
		//expect kcd to forward command to me
    p_msg = receive_message(&sender_id);
		release_memory_block(p_msg);
		if (sender_id == PID_KCD) {
			uart1_put_string("G005_test: test 1 OK\r\n");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 1 FAIL\n\r");
		}

		BOOL is_content_correct = TRUE;
		for (i = 0; i < 5; i++) {
			if (p_msg->mtext[i] != "%z a."[i]) {
				is_content_correct = FALSE;
			}
		}
		
		if (is_content_correct) {
			uart1_put_string("G005_test: test 2 OK\r\n");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 2 FAIL\n\r");
		}

		//pretending to be a process to register invalid command to KCD
    p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KCD_REG;
		p_msg->mtext[0] = '%';
		p_msg->mtext[1] = '1';
		p_msg->mtext[2] = '\0';
    send_message(PID_KCD, (void *) p_msg);
		
		//pretending to be a process send not registered command to KCD
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '%';
    send_message(PID_KCD, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = 'd';
    send_message(PID_KCD, (void *) p_msg);
		
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '.';
    send_message(PID_KCD, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '\n';
    send_message(PID_KCD, (void *) p_msg);
		
		//expect msg type fail
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = CRT_DISPLAY;
		p_msg->mtext[0] = '\n';
    send_message(PID_KCD, (void *) p_msg);
		
		//pretending to be a process send invalid command to KCD: does not start with '%'
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = 'a';
    send_message(PID_KCD, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = 'z';
    send_message(PID_KCD, (void *) p_msg);
		
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '.';
    send_message(PID_KCD, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '\n';
    send_message(PID_KCD, (void *) p_msg);
    while (1) {
        //uart1_put_string("proc1:\n\r");
        release_processor();
    }
}
