#ifndef K_PROCESS_B_H_
#define K_PROCESS_B_H_

#include "common.h"
#include "rtx.h"
#include "uart_polling.h"

// the process B
extern void proc_b(void);

#endif
