#include "queue.h"
#include "common.h"


/**************************************************************************//**
 * @brief   enqueue(queue* q, node* n)
 * @return  RTX_ERR on error and RTX_OK on success
 * @post    the node n is added in the end of the queue
 *****************************************************************************/
int enqueue(queue* q, node* n) {
	if (NULL == q || NULL == n) {
		return RTX_ERR;
	}

	if (q->size == 0) {
		q->head = n;
		q->tail = n;
		n->next = NULL;
	} else {
		q->tail->next = n;
		q->tail = n;
	}
	q->size++;
	return RTX_OK;
}

/**************************************************************************//**
 * @brief   enqueue_opposite(queue* q, node* n)
 * @return  RTX_ERR on error and RTX_OK on success
 * @post    the node n is added in the front of the queue
 *****************************************************************************/
int enqueue_opposite(queue* q, node* n) {
	if (NULL == q || NULL == n) {
		return RTX_ERR;
	}

	if (q->size == 0) {
		q->head = n;
		q->tail = n;
		n->next = NULL;
	}
	else {
		n->next = q->head;
		q->head = n;
	}
	q->size++;
	return RTX_OK;
}

/**************************************************************************//**
 * @brief   dequeue(queue* q)
 * @return  NULL when queue is empty and the node on success
 * @post    head is poped 
 *****************************************************************************/
node* dequeue(queue* q) {
	if (q->size > 0) {
		node* tmp = q->head;
		q->head = q->head->next;
		if (q->head == NULL) {
				q->tail = NULL;
		}
		q->size--;
		return tmp;
	} else {
		return NULL;
	}
}

/**************************************************************************//**
 * @brief   is_equal(node* n1, node* n2)
 * @return  true when nodes are equal, false otherwise
 *****************************************************************************/
bool is_equal(node* n1, node* n2) {
	if (n1 != NULL && n2 != NULL
		&& *(n1->ptr) == *(n2->ptr)
		&& n1->value1 == n2->value1
		&& n1->value2 == n2->value2
		&& n1->value3 == n2->value3) {
		return true;
	}
	return false;
}

/**************************************************************************//**
 * @brief   queue_remove(queue* q, node *nodeTarget)
 * @return  RTX_ERR on error and RTX_OK on success
 * @post    head is poped
 *****************************************************************************/
int queue_remove(queue* q, node *nodeTarget) {
	// base case when size == 0
	if (q->size == 0) {
		return RTX_ERR;
	}

	// if head is the target, call dequeue directly
	if (is_equal(q->head, nodeTarget)) {
		dequeue(q);
		return RTX_OK;
	}

	node* tmpNode;
	tmpNode = q->head;

	while (tmpNode->next != NULL) {
		if (is_equal(tmpNode->next, nodeTarget)) {
			tmpNode->next = nodeTarget->next;
			q->size--;
			if (is_equal(q->tail, nodeTarget)) {
				q->tail = tmpNode;
			}
			return RTX_OK;
		}
		tmpNode = tmpNode->next;
	}
	return RTX_ERR;
}

/**************************************************************************
 * @brief   queue_isEmpty(queue* q)
 * @return  true if the queue is empty
 *****************************************************************************/

bool queue_isEmpty(queue* q) {
	return (q->size == 0) ? true : false;
}

/**************************************************************************
 * @brief   queue_getSize(queue* q)
 * @return  the size of the queue
 *****************************************************************************/

int queue_getSize(queue* q) {
	return q->size;
}

