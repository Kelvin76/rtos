/*
 ****************************************************************************
 *
 *                  UNIVERSITY OF WATERLOO SE 350 RTX LAB  
 *
 *                     Copyright 2020-2021 Yiqing Huang
 *                          All rights reserved.
 *---------------------------------------------------------------------------
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice and the following disclaimer.
 *
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *---------------------------------------------------------------------------*/
/**************************************************************************//**
 * @brief   KCD test
 * @author  JL. Yiqing Huang
 * @date    2020/03/27
 * @note    Each process is in an infinite loop. Processes never terminate.
 *****************************************************************************/

/*------------------------------------------------------------------------*
Remember to set preprocessor DEBUG_KCD for this to work
-------------------------------------------------------------------------*/

#include "ae_proc.h"
#include "ae.h"
#include "uart_polling.h"
#include "printf.h"

int ok_counter = 0;
int total_test = 11;

/* initialization table item */
void set_test_procs(PROC_INIT *procs, int num)
{
    int i;
    for( i = 0; i < num; i++ ) {
        procs[i].m_pid        = (U32)(i+1);
        procs[i].m_stack_size = 0x300;
    }
  
    procs[0].mpf_start_pc = &proc1;
    procs[0].m_priority   = MEDIUM;
    
    procs[1].mpf_start_pc = &proc2;
    procs[1].m_priority   = MEDIUM;
    
    procs[2].mpf_start_pc = &proc3;
    procs[2].m_priority   = LOW;
    
    procs[3].mpf_start_pc = &proc4;
    procs[3].m_priority   = LOWEST; 
    
    procs[4].mpf_start_pc = &proc5;
    procs[4].m_priority   = LOWEST;
    
    procs[5].mpf_start_pc = &proc6;
    procs[5].m_priority   = LOWEST;
    
}

/**
 * @brief process 1 pretends to be UART I proc to 
		send key strokes to KCD
 */
void proc1(void)
{
    MSG_BUF *p_msg;
    int     sender_id;
		int i;
    
    uart1_put_string("proc1: entering...\n\r");
		//pretending to be a process to register command to KCD
    p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KCD_REG;
		p_msg->mtext[0] = '%';
		p_msg->mtext[1] = 'z';
		p_msg->mtext[2] = '\0';
    send_message(PID_KCD, (void *) p_msg);
	
		//pretending to be UART I, sending key stroke to kcd
    p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '%';
    send_message(PID_KCD, (void *) p_msg);

    p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = 'z';
    send_message(PID_KCD, (void *) p_msg);
		
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = ' ';
    send_message(PID_KCD, (void *) p_msg);
		
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = 'a';
    send_message(PID_KCD, (void *) p_msg);
		
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '.';
    send_message(PID_KCD, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '\n';
    send_message(PID_KCD, (void *) p_msg);
		
		//expect kcd to forward command to me
    p_msg = receive_message(&sender_id);
		release_memory_block(p_msg);
		if (sender_id == PID_KCD) {
			uart1_put_string("G005_test: test 1 OK\r\n");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 1 FAIL\n\r");
		}

		BOOL is_content_correct = TRUE;
		for (i = 0; i < 5; i++) {
			if (p_msg->mtext[i] != "%z a."[i]) {
				is_content_correct = FALSE;
			}
		}
		
		if (is_content_correct) {
			uart1_put_string("G005_test: test 2 OK\r\n");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 2 FAIL\n\r");
		}

		//pretending to be a process to register invalid command to KCD
    p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KCD_REG;
		p_msg->mtext[0] = '%';
		p_msg->mtext[1] = '1';
		p_msg->mtext[2] = '\0';
    send_message(PID_KCD, (void *) p_msg);
		
		//pretending to be a process send not registered command to KCD
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '%';
    send_message(PID_KCD, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = 'd';
    send_message(PID_KCD, (void *) p_msg);
		
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '.';
    send_message(PID_KCD, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '\n';
    send_message(PID_KCD, (void *) p_msg);
		
		//expect msg type fail
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = CRT_DISPLAY;
		p_msg->mtext[0] = '\n';
    send_message(PID_KCD, (void *) p_msg);
		
		//pretending to be a process send invalid command to KCD: does not start with '%'
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = 'a';
    send_message(PID_KCD, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = 'z';
    send_message(PID_KCD, (void *) p_msg);
		
		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '.';
    send_message(PID_KCD, (void *) p_msg);

		p_msg = (MSG_BUF *)request_memory_block();
    p_msg->mtype = KEY_IN;
		p_msg->mtext[0] = '\n';
    send_message(PID_KCD, (void *) p_msg);
    while (1) {
        //uart1_put_string("proc1:\n\r");
        release_processor();
    }
}

/**
 * @brief process 2 pretends to be CRT, receive msg sent by 
		KCD.
 */

void proc2(void)
{
    MSG_BUF *p_msg;
    int     sender_id;
    
    uart1_put_string("proc2: entering...\n\r");
    p_msg = receive_message(&sender_id);
		if (sender_id == PID_KCD && p_msg->mtext[0] == '%') {
			uart1_put_string("G005_test: test 3 OK\r\n");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 3 FAIL\n\r");
		}
		release_memory_block(p_msg);
		
    p_msg = receive_message(&sender_id);
		if (sender_id == PID_KCD && p_msg->mtext[0] == 'z') {
			uart1_put_string("G005_test: test 4 OK\r\n");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 4 FAIL\n\r");
		}
		release_memory_block(p_msg);

    p_msg = receive_message(&sender_id);
		if (sender_id == PID_KCD && p_msg->mtext[0] == ' ') {
			uart1_put_string("G005_test: test 5 OK\r\n");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 5 FAIL\n\r");
		}
		release_memory_block(p_msg);

    p_msg = receive_message(&sender_id);
		if (sender_id == PID_KCD && p_msg->mtext[0] == 'a') {
			uart1_put_string("G005_test: test 6 OK\r\n");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 6 FAIL\n\r");
		}
		release_memory_block(p_msg);

    p_msg = receive_message(&sender_id);
		if (sender_id == PID_KCD && p_msg->mtext[0] == '.') {
			uart1_put_string("G005_test: test 7 OK\r\n");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 7 FAIL\n\r");
		}
		release_memory_block(p_msg);

    p_msg = receive_message(&sender_id);
		if (sender_id == PID_KCD && p_msg->mtext[0] == '\n') {
			uart1_put_string("G005_test: test 8 OK\r\n");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 8 FAIL\n\r");
		}
		release_memory_block(p_msg);
		
		//expect invalid command reg error
    p_msg = receive_message(&sender_id);
		BOOL is_content_correct = TRUE;
		for (int i = 0; i < 38; i++) {
			if (p_msg->mtext[i] != "KCD Error: cmd registration invalid\r\n"[i]) {
				is_content_correct = FALSE;
			}
		}
		if (sender_id == PID_KCD && is_content_correct) {
			uart1_put_string("G005_test: test 9 OK\n\r");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 9 FAIL\n\r");
		}
		release_memory_block(p_msg);
	
		//expect invalid command error
		//skip the four echoes
    p_msg = receive_message(&sender_id);
		release_memory_block(p_msg);		
    p_msg = receive_message(&sender_id);
		release_memory_block(p_msg);
    p_msg = receive_message(&sender_id);
		release_memory_block(p_msg);
    p_msg = receive_message(&sender_id);
		release_memory_block(p_msg);

    p_msg = receive_message(&sender_id);
		is_content_correct = TRUE;
		for (int i = 0; i < 20; i++) {
			if (p_msg->mtext[i] != "Command not found\r\n"[i]) {
				is_content_correct = FALSE;
			}
		}
		release_memory_block(p_msg);

		if (sender_id == PID_KCD && is_content_correct) {
			uart1_put_string("G005_test: test 10 OK\n\r");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 10 FAIL\n\r");
		}
		
		//expect invalid msg type error
    p_msg = receive_message(&sender_id);
		is_content_correct = TRUE;
		for (int i = 0; i < 29; i++) {
			if (p_msg->mtext[i] != "KCD Error: msg type invalid\r\n"[i]) {
				is_content_correct = FALSE;
			}
		}
		release_memory_block(p_msg);

		if (sender_id == PID_KCD && is_content_correct) {
			uart1_put_string("G005_test: test 11 OK\n\r");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 11 FAIL\n\r");
		}
		
		//expect invalid command error
		//skip the four echoes
    p_msg = receive_message(&sender_id);
		release_memory_block(p_msg);		
    p_msg = receive_message(&sender_id);
		release_memory_block(p_msg);
    p_msg = receive_message(&sender_id);
		release_memory_block(p_msg);
    p_msg = receive_message(&sender_id);
		release_memory_block(p_msg);

    p_msg = receive_message(&sender_id);
		is_content_correct = TRUE;
		for (int i = 0; i < 18; i++) {
			if (p_msg->mtext[i] != "Invalid command\r\n"[i]) {
				is_content_correct = FALSE;
			}
		}
		release_memory_block(p_msg);

		if (sender_id == PID_KCD && is_content_correct) {
			uart1_put_string("G005_test: test 12 OK\n\r");
			ok_counter++;
		} else {
			uart1_put_string("G005_test: test 12 FAIL\n\r");
		}
		
    while(1) {
        //uart1_put_string("proc2:\n\r");
        release_processor();
    }
}

void proc3(void)
{
    uart1_put_string("proc3:\n\r");
    while(1) {
        release_processor();
    }
}

void proc4(void)
{
    uart1_put_string("proc4:\n\r");
    while(1) {
        release_processor();
    }
}

void proc5(void)
{
    while(1) {
        release_processor();
    }
}

void proc6(void)
{
    while(1) {
        release_processor();
    }
}
